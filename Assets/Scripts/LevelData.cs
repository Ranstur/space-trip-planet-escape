﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelData : MonoBehaviour
{
    [Header("Resources")]
    public bool boughtAssemblyBench;
    public bool boughtModdingTable;
    public bool boughtUpgradeBench;
    public bool boughtBuildingBench;
    public bool boughtRecycler;
    public bool boughtArmorBench;
    public bool boughtRockBreaker;
    public bool boughtIronMine;
    public bool boughtFuelExtractor;
    [Header("Defences")]
    public bool deleteme;
}
