﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    [Header("Gun Holder")]
    [SerializeField] private Transform player;
    [Space]
    [SerializeField] private Transform leftHolder;
    [SerializeField] private Transform rightHolder;
    [Header("Shooting stats")]
    public bool auto;
    public bool semi;
    public float fireRate;

    private int gunPoint = 180;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            if (player.transform.rotation.y != leftHolder.transform.rotation.y)
            {
                player.transform.Rotate(0, gunPoint, 0);
            }
        }
        if(Input.GetKeyDown(KeyCode.D))
        {
            if(player.transform.rotation.y != rightHolder.transform.rotation.y)
            {
                player.transform.Rotate(0, -gunPoint, 0);
            }
        }
        if (auto)
        {
            if (Input.GetKey(KeyCode.Space))
            {

            }
        }
        if(semi)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {

            }
        }
    }
}
