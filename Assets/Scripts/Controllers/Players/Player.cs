﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private float heath = 100;
    [Space]
    [SerializeField] private Slider hpBar;
    [SerializeField] private Image fillBar;
    public bool haveArmor;
    public float absorbs;

    private Data dataBanks;

    private void Start()
    {
        dataBanks = FindObjectOfType<Data>();

        if (dataBanks.playerHeath != 0)
        {
            heath = dataBanks.playerHeath;
            SetBar(heath);
        }
        else
        {
            dataBanks.playerHeath = heath;
            SetBar(heath);
        }
    }
    public void TakeDamage(float damage)
    {
        if (haveArmor)
        {
            float newDamage = damage / absorbs;

            heath -= newDamage;
        }
        else
        {
            heath -= damage;
        }

        if(heath <= 0)
        {
            fillBar.enabled = false;
            Die();
        }

        SetBar(heath);

        dataBanks.playerHeath = heath;
    }
    private void SetBar(float heath)
    {
        float hp = heath / 100;
        hpBar.value = hp;
    }
    private void Die()
    {
        //Kill player
        //pop up a menu to respawn or quit
    }
}
