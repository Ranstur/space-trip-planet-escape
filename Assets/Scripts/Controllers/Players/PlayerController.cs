﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float movementSpeed;
    [SerializeField] private float rotationSpeed;
    [Header("Public bools")]
    public bool canMove = true;

    private bool swiftLock;
    [SerializeField] private SpriteRenderer sr;
    private Animator anim;
    private const string animParamBoolMoveUp = "MoveUp";
    private const string animParamBoolMoveDown = "MoveDown";
    private const string animParamBoolMoveRight = "MoveRight";
    private const string animParamBoolMoveLeft = "MoveLeft";

    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftAlt) && !swiftLock)
        {
            swiftLock = true;
        }
        else if(Input.GetKeyDown(KeyCode.LeftAlt) && swiftLock)
        {
            swiftLock = false;
        }
        if (canMove)
        {
            anim.SetBool(animParamBoolMoveDown, Input.GetKey(KeyCode.S));
            anim.SetBool(animParamBoolMoveUp, Input.GetKey(KeyCode.W));
            anim.SetBool(animParamBoolMoveRight, Input.GetKey(KeyCode.D));
            anim.SetBool(animParamBoolMoveLeft, Input.GetKey(KeyCode.A));

            if (swiftLock)
            {
                if (Input.GetKey(KeyCode.W))
                {
                    transform.Translate(0, movementSpeed * Time.deltaTime, 0);
                }
                if (Input.GetKey(KeyCode.S))
                {
                    transform.Translate(0, -movementSpeed * Time.deltaTime, 0);
                }
                if (Input.GetKey(KeyCode.D))
                {
                    transform.Translate(movementSpeed * Time.deltaTime, 0, 0);
                }
                if (Input.GetKey(KeyCode.A))
                {
                    transform.Translate(-movementSpeed * Time.deltaTime, 0, 0);
                }
            }
            if(!swiftLock)
            {
                if(Input.GetKey(KeyCode.W))
                {
                    transform.Translate(0, movementSpeed * Time.deltaTime, 0);
                }
                if(Input.GetKey(KeyCode.A))
                {
                    transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
                }
                if(Input.GetKey(KeyCode.D))
                {
                    transform.Rotate(0, 0, -rotationSpeed * Time.deltaTime);
                }
                if(Input.GetKey(KeyCode.S))
                {
                    transform.Translate(0, -movementSpeed * Time.deltaTime, 0);
                }
            }
        }
    }
}
