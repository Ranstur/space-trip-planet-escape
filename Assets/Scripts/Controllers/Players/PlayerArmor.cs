﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArmor : MonoBehaviour
{
    public bool haveArmor;
    [Header("Leather Armor")]
    public bool leatherArmor;
    [SerializeField] private Sprite leatherUp;
    [SerializeField] private Sprite leatherDown;
    [SerializeField] private Sprite leatherRight;
    [SerializeField] private Sprite leatherLeft;
    [Header("Iron Armor")]
    public bool ironArmor;
    [SerializeField] private Sprite ironUp;
    [SerializeField] private Sprite ironDown;
    [SerializeField] private Sprite ironRight;
    [SerializeField] private Sprite ironLeft;
    [Header("Steel Armor")]
    public bool steelArmor;
    [SerializeField] private Sprite steelUp;
    [SerializeField] private Sprite steelDown;
    [SerializeField] private Sprite steelRight;
    [SerializeField] private Sprite steelLeft;
    [Header("Electric Armor")]
    public bool electricArmor;
    [SerializeField] private Sprite electricUp;
    [SerializeField] private Sprite electricDown;
    [SerializeField] private Sprite electricRight;
    [SerializeField] private Sprite electricLeft;
    [Header("Super Electric Armor")]
    public bool superElectricArmor;
    [SerializeField] private Sprite superElectricUp;
    [SerializeField] private Sprite superElectricDown;
    [SerializeField] private Sprite superElectricRight;
    [SerializeField] private Sprite superElectricLeft;
    [Header("Refs")]
    [SerializeField] private SpriteRenderer sr;

    private Player player;

    private void FixedUpdate()
    {
        player = GetComponent<Player>();

        if(haveArmor)
        {
            sr.enabled = true;

            if(leatherArmor)
            {
                player.haveArmor = true;
                player.absorbs = 10;

                if (Input.GetKey(KeyCode.W))
                {
                    sr.sprite = leatherUp;
                }
                if (Input.GetKey(KeyCode.S))
                {
                    sr.sprite = leatherDown;
                }
                if (Input.GetKey(KeyCode.A))
                {
                    sr.sprite = leatherLeft;
                }
                if (Input.GetKey(KeyCode.D))
                {
                    sr.sprite = leatherRight;
                }
                if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
                {
                    sr.sprite = leatherDown;
                }
            }
            if(ironArmor)
            {
                player.haveArmor = true;
                player.absorbs = 20;

                if (Input.GetKey(KeyCode.W))
                {
                    sr.sprite = ironUp;
                }
                if (Input.GetKey(KeyCode.S))
                {
                    sr.sprite = ironDown;
                }
                if (Input.GetKey(KeyCode.A))
                {
                    sr.sprite = ironLeft;
                }
                if (Input.GetKey(KeyCode.D))
                {
                    sr.sprite = ironRight;
                }
                if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
                {
                    sr.sprite = ironDown;
                }
            }
            if(steelArmor)
            {
                player.haveArmor = true;
                player.absorbs = 30;

                if (Input.GetKey(KeyCode.W))
                {
                    sr.sprite = steelUp;
                }
                if (Input.GetKey(KeyCode.S))
                {
                    sr.sprite = steelDown;
                }
                if (Input.GetKey(KeyCode.A))
                {
                    sr.sprite = steelLeft;
                }
                if (Input.GetKey(KeyCode.D))
                {
                    sr.sprite = steelRight;
                }
                if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
                {
                    sr.sprite = steelDown;
                }
            }
            if(electricArmor)
            {
                player.haveArmor = true;
                player.absorbs = 40;

                if (Input.GetKey(KeyCode.W))
                {
                    sr.sprite = electricUp;
                }
                if (Input.GetKey(KeyCode.S))
                {
                    sr.sprite = electricDown;
                }
                if (Input.GetKey(KeyCode.A))
                {
                    sr.sprite = electricLeft;
                }
                if (Input.GetKey(KeyCode.D))
                {
                    sr.sprite = electricRight;
                }
                if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
                {
                    sr.sprite = electricDown;
                }
            }
            if(superElectricArmor)
            {
                player.haveArmor = true;
                player.absorbs = 50;

                if (Input.GetKey(KeyCode.W))
                {
                    sr.sprite = superElectricUp;
                }
                if (Input.GetKey(KeyCode.S))
                {
                    sr.sprite = superElectricDown;
                }
                if (Input.GetKey(KeyCode.A))
                {
                    sr.sprite = superElectricLeft;
                }
                if (Input.GetKey(KeyCode.D))
                {
                    sr.sprite = superElectricRight;
                }
                if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D))
                {
                    sr.sprite = superElectricDown;
                }
            }
        }
        else
        {
            sr.enabled = false;
        }
    }
}
