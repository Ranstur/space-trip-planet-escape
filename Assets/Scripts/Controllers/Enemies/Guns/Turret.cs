﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    [Header("modes")]
    public bool dayLight;
    public bool laserSite;
    public bool spotLight;
    [Header("Turret Settings")]
    [SerializeField] private float fireRate;
    [SerializeField] private float lookingTimer;
    [SerializeField] private Transform shotSpot;
    [SerializeField] private GameObject bulletPrefab;
    [Header("Hardware")]
    [SerializeField] private Transform hardwareMount;
    [Space]
    [SerializeField] private Transform laserMount;
    [SerializeField] private Transform spotLightMount;

    private bool attacking;

    private Stand stand;
    private Player player;

    private void Start()
    {
        stand = GetComponentInParent<Stand>();

        if(laserMount != null)
        {
            laserMount.transform.position = hardwareMount.transform.position;
            laserMount.transform.rotation = hardwareMount.transform.rotation;
        }
        if(spotLightMount != null)
        {
            spotLightMount.transform.position = hardwareMount.transform.position;
            spotLightMount.transform.rotation = hardwareMount.transform.rotation;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        player = collision.GetComponent<Player>();
        if(player != null && !attacking)
        {
            Attack();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        player = collision.GetComponent<Player>();
        if(player != null)
        {
            attacking = false;
            StopAllCoroutines();
            StartCoroutine(Looking());
        }
    }
    private void Attack()
    {
        attacking = true;

        stand.attack = true;
        stand.locked = false;
        stand.target = player.transform;

        StartCoroutine(Shooting());
        StopCoroutine(Looking());
    }
    private void StopShooting()
    {
        attacking = false;
        stand.attack = false;
        stand.target = null;

        //StartCoroutine(stand.Rotation());
    }
    private IEnumerator Shooting()
    {
        GameObject bullet = Instantiate(bulletPrefab);
        bullet.transform.position = shotSpot.transform.position;
        bullet.transform.rotation = shotSpot.transform.rotation;

        yield return new WaitForSeconds(fireRate);

        StartCoroutine(Shooting());
    }
    private IEnumerator Looking()
    {
        yield return new WaitForSeconds(lookingTimer);

        StopShooting();
    }
}
