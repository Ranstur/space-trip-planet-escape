﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stand : MonoBehaviour
{
    [SerializeField] private Vector3 topPoint;
    [SerializeField] private Vector3 bottomPoint;
    [Space]
    [SerializeField] private float speed;
    [SerializeField] private float delay = 1;
    [Header("Mountes")]
    [SerializeField] private Transform standMount;
    [SerializeField] private Transform weaponMount;

    public bool attack;
    public Transform target;
    public bool locked = false;
    private float curretRotation = 0;
    private bool top;

    private void Start()
    {
        weaponMount.transform.position = standMount.transform.position;
        weaponMount.transform.rotation = standMount.transform.rotation;

        //StartCoroutine(Rotation());
    }
    private void Update()
    {
        if (!locked)
        {
            StartCoroutine(Rotation());
            locked = true;
        }
        if(curretRotation >= topPoint.z)
        {
            top = false;
            curretRotation = topPoint.z;
        }
        if(curretRotation <= bottomPoint.z)
        {
            top = true;
            curretRotation = bottomPoint.z;
        }
    }
   public IEnumerator Rotation()
    {
        if (!attack)
        {
            if (top)
            {
                curretRotation += speed;
                weaponMount.transform.Rotate(0, 0, +speed);
            }
            else
            {
                curretRotation -= speed;
                weaponMount.transform.Rotate(0, 0, -speed);
            }

        }
        yield return new WaitForSeconds(delay);

        StartCoroutine(Rotation());
    }
}
