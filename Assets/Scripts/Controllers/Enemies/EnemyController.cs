﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private float movementSpeed;
    [SerializeField] private List<Transform> patrolNodes;

    private int targetNodeIndex;

    private void Start()
    {
        transform.position = patrolNodes[targetNodeIndex].position;        
    }
    private void Update()
    {
        Transform targetNode = patrolNodes[targetNodeIndex].transform;
        float distance = Vector2.Distance(transform.position, targetNode.transform.position);
        
        if(distance <= 0.1f)
        {
            targetNodeIndex++;

            if(targetNodeIndex >= patrolNodes.Count)
            {
                targetNodeIndex = 0;
            }
        }
        else
        {
            transform.position = Vector2.MoveTowards(transform.position, targetNode.position, movementSpeed * Time.deltaTime);
        }
    }
    //private void OnDrawGizmos()
    //{
    //    if(patrolNodes.All(p => p != null))
    //    {
    //        UnityEditor.Handles.DrawPolyLine(patrolNodes.Select(p => p.position).ToArray());
    //    }
    //}
}
