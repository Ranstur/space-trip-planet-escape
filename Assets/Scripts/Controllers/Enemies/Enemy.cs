﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float heath;

    public void TakeDamage(float damage)
    {
        heath -= damage;

        if(heath <= 0)
        {
            Die();
        }
    }
    private void Die()
    {
        //want enemy lay died for abit then destroy it
        Destroy(gameObject);
    }
}
