﻿using UnityEngine;

public class Data : MonoBehaviour
{
    [Header("Player Stats")]
    public float playerHeath;
    [Space]
    [Range(0, 9999999)] public int rocks;
    [Range(0, 9999999)] public int irons;
    [Range(0, 9999999)] public int fuels;
    [Header("SunCycle")]
    public float dayTimeLighting;
    public bool sunRising;
    [Header("GettingAWeapon")]
    public bool pistol;
    public bool assaultRifle;
    public bool sniper;
    [Space]
    public bool mainFirstObjectiveDone;
    public bool mainSecondObjectiveDone;
    public bool mainThreeObjectiveDone;
    public bool mainFourObjectiveDone;
    public bool mainFiveObjectiveDone;
    public bool mainSixObjectiveDone;
    public bool mainSevenObjectiveDone;
    public bool mainEightObjectiveDone;
    [Space]
    public bool firstObjectiveDone;
    public bool secondObjectiveDone;
}
