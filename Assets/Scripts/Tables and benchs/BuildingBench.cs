﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingBench : MonoBehaviour
{
    [SerializeField] private GameObject benchUI;

    private bool isIn;
    private bool inUse;

    private void Update()
    {
        PlayerController pc = FindObjectOfType<PlayerController>();

        if(Input.GetKeyDown(KeyCode.E) && isIn && !inUse)
        {
            inUse = true;
            benchUI.SetActive(true);
            pc.canMove = false;
        }
        else if(Input.GetKeyDown(KeyCode.E) && inUse)
        {
            inUse = false;
            benchUI.SetActive(false);
            pc.canMove = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if(player != null)
        {
            isIn = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if(player != null)
        {
            isIn = false;
        }
    }
}
