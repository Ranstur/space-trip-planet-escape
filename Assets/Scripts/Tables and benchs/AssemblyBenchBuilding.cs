﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AssemblyBenchBuilding : MonoBehaviour
{
    public bool pistol;
    public bool assaultRifle;
    public bool sniper;
    [Space]
    [SerializeField] private Button buildButton;
    [Space]
    [SerializeField] private Transform chamberSlot;
    [SerializeField] private Transform handleSlot;
    [SerializeField] private Transform barrelSlot;
    [SerializeField] private Transform armSupportSlot;
    [Space]
    [SerializeField] private GameObject weapon;
    [SerializeField] private Transform weaponSlot;
    [Header("NO NOT CHANGE!")]
    public GameObject savedChamber;
    public GameObject savedHandle;
    public GameObject savedBarrel;
    public GameObject savedArmSupport;

    public GameObject savedChamberButton;
    public GameObject savedHandleButton;
    public GameObject savedBarrelButton;
    public GameObject savedArmSupportButton;

    public bool chamberSlotFull;
    public bool handleSlotFull;
    public bool barrelSlotFull;
    public bool armSupportSlotFull;

    public Data data;
    public InventoryData inventData;

    private void Start()
    {
        buildButton.enabled = false;
        buildButton.onClick.AddListener(OnBuild);
    }
    private void Update()
    {
        pistol = data.pistol;
        assaultRifle = data.assaultRifle;
        sniper = data.sniper;

        if(pistol)
        {
            if(chamberSlotFull && barrelSlotFull && handleSlotFull)
            {
                buildButton.enabled = true;
            }
            else
            {
                buildButton.enabled = false;
            }
        }
        if(assaultRifle)
        {
            if (chamberSlotFull && barrelSlotFull && handleSlotFull && armSupportSlotFull)
            {
                buildButton.enabled = true;
            }
            else
            {
                buildButton.enabled = false;
            }
        }
        if(sniper)
        {
            if (chamberSlotFull && barrelSlotFull && handleSlotFull && armSupportSlotFull)
            {
                buildButton.enabled = true;
            }
            else
            {
                buildButton.enabled = false;
            }
        }

    }
    private void OnBuild()
    {
        Debug.Log("Building Gun!");

        data.secondObjectiveDone = true;
        //add the gun into your inventory.

        GameObject gun = Instantiate(weapon);
        Gun gunInfo = gun.GetComponent<Gun>();

        gun.transform.parent = weaponSlot.transform;

        chamberSlotFull = false;
        barrelSlotFull = false;
        handleSlotFull = false;

        AssemblyBenchItem chamber = savedChamberButton.GetComponent<AssemblyBenchItem>();
        AssemblyBenchItem barrel = savedBarrelButton.GetComponent<AssemblyBenchItem>();
        AssemblyBenchItem handle = savedHandleButton.GetComponent<AssemblyBenchItem>();


        if(pistol)
        {
            gunInfo.pistol = true;
        }
        if(assaultRifle)
        {
            gunInfo.assaultRifle = true;
        }
        if(sniper)
        {
            gunInfo.sniper = true;
        }

        if(chamber.chamber)
        {
            inventData.totalParts -= 1;
            inventData.chamber -= 1;
            gunInfo.chamber = true;
        }
        if(chamber.semiChamber)
        {
            inventData.totalParts -= 1;
            inventData.semiChamber -= 1;
            gunInfo.semiChamber = true;
        }
        if(chamber.autoChamber)
        {
            inventData.totalParts -= 1;
            inventData.autoChamber -= 1;
            gunInfo.autoChamber = true;
        }

        if(barrel.barrel)
        {
            inventData.totalParts -= 1;
            inventData.barrel -= 1;
            gunInfo.barrel = true;
        }
        if(barrel.barrelRanger)
        {
            inventData.totalParts -= 1;
            inventData.barrelRanger -= 1;
            gunInfo.barrelRanger = true;
        }
        if(barrel.silencerBarrel)
        {
            inventData.totalParts -= 1;
            inventData.silencerBarrel -= 1;
            gunInfo.silencerBarrel = true;
        }

        if(handle.handle)
        {
            inventData.totalParts -= 1;
            inventData.handle -= 1;
            gunInfo.handle = true;
        }
        if(handle.semiHandle)
        {
            inventData.totalParts -= 1;
            inventData.semiHandle -= 1;
            gunInfo.semiHandle = true;
        }
        if(handle.autoHandle)
        {
            inventData.totalParts -= 1;
            inventData.autoHandle -= 1;
            gunInfo.autoHandle = true;
        }
        if (!pistol)
        {
            armSupportSlotFull = false;
            AssemblyBenchItem armSupport = savedArmSupportButton.GetComponent<AssemblyBenchItem>();

            if (armSupport.armSupport)
            {
                inventData.totalParts -= 1;
                inventData.armSupport -= 1;
                gunInfo.armSupport = true;
            }
            if (armSupport.autoArmSupport)
            {
                inventData.totalParts -= 1;
                inventData.autoArmSupport -= 1;
                gunInfo.autoArmSupport = true;
            }
            if (armSupport.recoilArmSuppot)
            {
                inventData.totalParts -= 1;
                inventData.recoilArmSupport -= 1;
                gunInfo.recoilArmSupport = true;
            }
        }

        Destroy(savedArmSupport);
        Destroy(savedArmSupportButton);
        Destroy(savedBarrel);
        Destroy(savedBarrelButton);
        Destroy(savedChamber);
        Destroy(savedChamberButton);
        Destroy(savedHandle);
        Destroy(savedHandleButton);
    }
    public void SetSlots(int amount, GameObject item, GameObject prefab)
    {
        if (amount == 1)
        {
            //Chamber
            if (!chamberSlotFull)
            {
                item.transform.parent = chamberSlot.transform;
                item.transform.position = chamberSlot.position;
                
                chamberSlotFull = true;
                savedChamber = item;
                savedChamberButton = prefab;
            }
            else
            {
                Destroy(savedChamber);
                savedChamberButton = null;

                item.transform.parent = chamberSlot.transform;
                item.transform.position = chamberSlot.position;

                chamberSlotFull = true;
                savedChamber = item;
                savedChamberButton = prefab;
            }
        }
        if(amount == 2)
        {
            //handle
            if(!handleSlotFull)
            {
                item.transform.parent = handleSlot.transform;
                item.transform.position = handleSlot.position;

                handleSlotFull = true;
                savedHandle = item;
                savedHandleButton = prefab;
            }
            else
            {
                Destroy(savedHandle);
                savedHandleButton = null;

                item.transform.parent = handleSlot.transform;
                item.transform.position = handleSlot.position;

                handleSlotFull = true;
                savedHandle = item;
                savedHandleButton = prefab;
            }
        }
        if(amount == 3)
        {
            //barrel
            if(!barrelSlotFull)
            {
                item.transform.parent = barrelSlot.transform;
                item.transform.position = barrelSlot.position;

                barrelSlotFull = true;
                savedBarrel = item;
                savedBarrelButton = prefab;
            }
            else
            {
                Destroy(savedBarrel);
                savedBarrelButton = null;

                item.transform.parent = barrelSlot.transform;
                item.transform.position = barrelSlot.position;

                barrelSlotFull = true;
                savedBarrel = item;
                savedBarrelButton = prefab;
            }
        }
        if (!pistol)
        {
            if (amount == 4)
            {
                //arm support
                if (!armSupportSlotFull)
                {
                    item.transform.parent = armSupportSlot.transform;
                    item.transform.position = armSupportSlot.position;

                    armSupportSlotFull = true;
                    savedArmSupport = item;
                    savedArmSupportButton = prefab;
                }
                else
                {
                    Destroy(savedArmSupport);
                    savedArmSupportButton = null;

                    item.transform.parent = armSupportSlot.transform;
                    item.transform.position = armSupportSlot.position;

                    armSupportSlotFull = true;
                    savedArmSupport = item;
                    savedArmSupportButton = prefab;
                }
            }
        }
    }
}
