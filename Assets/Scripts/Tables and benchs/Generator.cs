﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    public bool running;
    public bool makingPower;
    [Space]
    [SerializeField] private float startUpTimer;
    [SerializeField] private float shutDownTimer;
    [Space]
    [SerializeField] private Sprite redLight;
    [SerializeField] private Sprite greenLight;
    [Space]
    [SerializeField] private SpriteRenderer sr;

    private bool locked;

    private void Update()
    {
        if(running && !locked)
        {
            locked = true;
            StartCoroutine(Starting());
        }
        else if(!running && locked)
        {
            locked = false;
            StopAllCoroutines();
            Stopping();
        }
    }
    IEnumerator Starting()
    {
        yield return new WaitForSeconds(startUpTimer);

        sr.sprite = greenLight;

        makingPower = true;
    }
    private void Stopping()
    {
        sr.sprite = redLight;

        makingPower = false;
    }
}
