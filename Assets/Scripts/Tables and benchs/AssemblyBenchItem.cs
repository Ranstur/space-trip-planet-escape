﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AssemblyBenchItem : MonoBehaviour
{
    [SerializeField] private Button itemButton;
    [Space]
    public bool chamber;
    public bool semiChamber;
    public bool autoChamber;
    public bool handle;
    public bool semiHandle;
    public bool autoHandle;
    public bool barrel;
    public bool barrelRanger;
    public bool silencerBarrel;
    public bool armSupport;
    public bool autoArmSupport;
    public bool recoilArmSuppot;
    [Space]
    [SerializeField] private GameObject chamberPrefab;
    [SerializeField] private GameObject semiChamberPrefab;
    [SerializeField] private GameObject autoChamberPrefab;
    [SerializeField] private GameObject handlePrefab;
    [SerializeField] private GameObject semiHandlePrefab;
    [SerializeField] private GameObject autoHandlePrefab;
    [SerializeField] private GameObject barrelPrefab;
    [SerializeField] private GameObject barrelRangerPrefab;
    [SerializeField] private GameObject silencerBarrelPrefab;
    [SerializeField] private GameObject armSupportPrefab;
    [SerializeField] private GameObject autoArmSupportPrefab;
    [SerializeField] private GameObject recoilArmSupportPrefab;

    private AssemblyBenchBuilding building;

    private void Start()
    {
        building = FindObjectOfType<AssemblyBenchBuilding>();

        itemButton.onClick.AddListener(OnItem);
    }
    private void OnItem()
    {
        if(chamber)
        {
            GameObject item = Instantiate(chamberPrefab);
            building.SetSlots(1,item, gameObject);
        }
        if(semiChamber)
        {
            GameObject item = Instantiate(semiChamberPrefab);
            building.SetSlots(1, item, gameObject);
        }
        if(autoChamber)
        {
            GameObject item = Instantiate(autoChamberPrefab);
            building.SetSlots(1, item, gameObject);
        }
        if(handle)
        {
            GameObject item = Instantiate(handlePrefab);
            building.SetSlots(2, item, gameObject);
        }
        if(semiHandle)
        {
            GameObject item = Instantiate(semiHandlePrefab);
            building.SetSlots(2, item, gameObject);
        }
        if(autoHandle)
        {
            GameObject item = Instantiate(autoHandlePrefab);
            building.SetSlots(2, item, gameObject);
        }
        if(barrel)
        {
            GameObject item = Instantiate(barrelPrefab);
            building.SetSlots(3, item, gameObject);
        }
        if(barrelRanger)
        {
            GameObject item = Instantiate(barrelRangerPrefab);
            building.SetSlots(3, item, gameObject);
        }
        if(silencerBarrel)
        {
            GameObject item = Instantiate(silencerBarrelPrefab);
            building.SetSlots(3, item, gameObject);
        }
        if(armSupport)
        {
            GameObject item = Instantiate(armSupportPrefab);
            building.SetSlots(4, item, gameObject);
        }
        if(autoArmSupport)
        {
            GameObject item = Instantiate(autoArmSupportPrefab);
            building.SetSlots(4, item, gameObject);
        }
        if(recoilArmSuppot)
        {
            GameObject item = Instantiate(recoilArmSupportPrefab);
            building.SetSlots(4, item, gameObject);
        }
    }
}
