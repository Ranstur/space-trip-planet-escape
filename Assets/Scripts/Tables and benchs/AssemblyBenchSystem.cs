﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssemblyBenchSystem : MonoBehaviour
{
    [SerializeField] private Transform parentObject;
    [Space]
    [SerializeField] private GameObject chamberPrefab;
    [SerializeField] private GameObject semiChamberPrefab;
    [SerializeField] private GameObject autoChamberPrefab;
    [Space]
    [SerializeField] private GameObject handlePrefab;
    [SerializeField] private GameObject semiHandlePrefab;
    [SerializeField] private GameObject autoHandlePrefab;
    [Space]
    [SerializeField] private GameObject barrelPrefab;
    [SerializeField] private GameObject barrelRangerPrefab;
    [SerializeField] private GameObject silencerBarrelPrefab;
    [Space]
    [SerializeField] private GameObject armSupportPrefab;
    [SerializeField] private GameObject autoArmSupportPrefab;
    [SerializeField] private GameObject recoilArmSupportPrefab;

    private int chamber;
    private int semiChamber;
    private int autoChamber;
    private int handle;
    private int semiHandle;
    private int autoHandle;
    private int barrel;
    private int barrelRanger;
    private int silencerBarrel;
    private int armSupport;
    private int autoArmSupport;
    private int recoilArmSupport;

    public bool opened;
    private bool firstOpen;
    private int target;

    public List<GameObject> items;

    private InventoryData inventData;
    private AssemblyBench bench;
    private AssemblyBenchBuilding ABB;

    private void Start()
    {
        inventData = FindObjectOfType<InventoryData>();
        bench = FindObjectOfType<AssemblyBench>();
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine(Closing());
        }

        if (!firstOpen)
        {
            opened = bench.inUse;

            chamber = inventData.chamber;
            semiChamber = inventData.semiChamber;
            autoChamber = inventData.autoChamber;

            handle = inventData.handle;
            semiHandle = inventData.semiHandle;
            autoHandle = inventData.autoHandle;

            barrel = inventData.barrel;
            barrelRanger = inventData.barrelRanger;
            silencerBarrel = inventData.silencerBarrel;

            armSupport = inventData.armSupport;
            autoArmSupport = inventData.autoArmSupport;
            recoilArmSupport = inventData.recoilArmSupport;

            firstOpen = true;
        }

        if(opened)
        {
            if (chamber > 0)
            {
                GameObject item = Instantiate(chamberPrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                chamber -= 1;
            }
            if (semiChamber > 0)
            {
                GameObject item = Instantiate(semiChamberPrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                semiChamber -= 1;
            }
            if (autoChamber > 0)
            {
                GameObject item = Instantiate(autoChamberPrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                autoChamber -= 1;
            }
            if (handle > 0)
            {
                GameObject item = Instantiate(handlePrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                handle -= 1;
            }
            if (semiHandle > 0)
            {
                GameObject item = Instantiate(semiHandlePrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                semiHandle -= 1;
            }
            if (autoHandle > 0)
            {
                GameObject item = Instantiate(autoHandlePrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                autoHandle -= 1;
            }
            if (barrel > 0)
            {
                GameObject item = Instantiate(barrelPrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                barrel -= 1;
            }
            if (barrelRanger > 0)
            {
                GameObject item = Instantiate(barrelRangerPrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                barrelRanger -= 1;
            }
            if (silencerBarrel > 0)
            {
                GameObject item = Instantiate(silencerBarrelPrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                silencerBarrel -= 1;
            }
            if (armSupport > 0)
            {
                GameObject item = Instantiate(armSupportPrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                armSupport -= 1;
            }
            if (autoArmSupport > 0)
            {
                GameObject item = Instantiate(autoArmSupportPrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                autoArmSupport -= 1;
            }
            if (recoilArmSupport > 0)
            {
                GameObject item = Instantiate(recoilArmSupportPrefab);
                item.transform.parent = parentObject.transform;

                items.Add(item);

                recoilArmSupport -= 1;
            }
        }
    }
    IEnumerator Closing()
    {
        ABB = GetComponent<AssemblyBenchBuilding>();
        Destroy(ABB.savedArmSupport);
        ABB.savedArmSupportButton = null;
        Destroy(ABB.savedBarrel);
        ABB.savedBarrelButton = null;
        Destroy(ABB.savedChamber);
        ABB.savedChamberButton = null;
        Destroy(ABB.savedHandle);
        ABB.savedHandleButton = null;

        ABB.chamberSlotFull = false;
        ABB.barrelSlotFull = false;
        ABB.handleSlotFull = false;
        ABB.armSupportSlotFull = false;

        if(items.Count == 0)
        {
            bench.canClose = true;
            opened = false;
            firstOpen = false;
        }
        else Destroy(items[target]);

        yield return new WaitForSeconds(0.001f);

        if (target < items.Count)
        {
            target++;

            StartCoroutine(Closing());
        }
        if(target >= items.Count)
        {
            target = 0;
            items.Clear();
            bench.canClose = true;
            opened = false;
            firstOpen = false;
        }
    }
    //here it will take the info from inventoryData and spawn in the parts.

    //when you close it it will detroy all parts, so when you reopen it it will not make dobles.

    //when you build a gun it will -part from the inventoryData.

    //the build button will turn on when you put all the parts into the parts slots.
}
