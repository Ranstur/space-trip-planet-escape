﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collecter : MonoBehaviour
{
    [SerializeField] private bool rock;
    [SerializeField] private bool iron;
    [SerializeField] private bool fuel;
    [SerializeField] private int amount;
    [Space]
    [SerializeField] private float collectionTimer;

    private Data data;

    private void Start()
    {
        data = FindObjectOfType<Data>();

        StartCoroutine(Collecting());
    }
    IEnumerator Collecting()
    {
        yield return new WaitForSeconds(collectionTimer);

        if(rock)
        {
            data.rocks += amount;
        }
        if(iron)
        {
            data.irons += amount;
        }
        if(fuel)
        {
            data.fuels += amount;
        }

        StartCoroutine(Collecting());
    }
}
