﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AssemblyBench : MonoBehaviour
{
    [SerializeField] private GameObject backgroundUI;
    [SerializeField] private GameObject text;
    [Space]
    [SerializeField] private GameObject pistolRecipe;
    [SerializeField] private GameObject assaultRifleRecipe;
    [SerializeField] private GameObject sniperRecipe;
    [Space]
    [SerializeField] private GameObject pistolButtonUI;
    [SerializeField] private GameObject AssaultRifleButtonUI;
    [SerializeField] private GameObject sniperButtonUI;
    [Space]
    [SerializeField] private Button pistolButton;
    [SerializeField] private Button assaultRifleButton;
    [SerializeField] private Button sniperButton;

    private bool pistol;
    private bool assaultRifle;
    private bool sniper;

    private bool isIn;
    public bool inUse;
    public bool canClose;

    private Data data;
    private PlayerController pc;
    public AssemblyBenchSystem system;

    private void Start()
    {
        pistolRecipe.SetActive(false);
        assaultRifleRecipe.SetActive(false);
        sniperRecipe.SetActive(false);

        pistolButton.onClick.AddListener(PistolRecipe);
        assaultRifleButton.onClick.AddListener(AssaultRifleRecipe);
        sniperButton.onClick.AddListener(SniperRecipe);
    }
    private void Update()
    {
        pc = FindObjectOfType<PlayerController>();

        if(Input.GetKeyDown(KeyCode.E) && isIn && !inUse)
        {
            system.opened = true;

            inUse = true;
            backgroundUI.SetActive(true);
            pc.canMove = false;
        }
        if(canClose)
        {
            inUse = false;
            backgroundUI.SetActive(false);
            pc.canMove = true;
            canClose = false;

            pistolRecipe.SetActive(false);
            assaultRifleRecipe.SetActive(false);
            sniperRecipe.SetActive(false);
        }
    }
    private void FixedUpdate()
    {
        data = FindObjectOfType<Data>();

        pistol = data.pistol;
        assaultRifle = data.assaultRifle;
        sniper = data.sniper;

        if (!pistol && !assaultRifle && !sniper)
        {
            text.SetActive(true);
        }
        else text.SetActive(false);

        if(pistol)
        {
            pistolButtonUI.SetActive(true);
        }
        if(assaultRifle)
        {
            AssaultRifleButtonUI.SetActive(true);
        }
        if(sniper)
        {
            sniperButtonUI.SetActive(true);
        }
    }
    private void PistolRecipe()
    {
        pistolRecipe.SetActive(true);
    }
    private void AssaultRifleRecipe()
    {
        assaultRifleRecipe.SetActive(true);
    }
    private void SniperRecipe()
    {
        sniperRecipe.SetActive(true);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if(player != null)
        {
            isIn = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if(player != null)
        {
            isIn = false;
        }
    }
}
