﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SupplyTank : MonoBehaviour
{
    [Header("Tank Settings")]
    [SerializeField] private bool isFuel;
    [SerializeField] private bool isIron;
    [SerializeField] private bool isRock;
    [Space]
    [SerializeField] private int amount;
    [Header("other stuff")]
    [SerializeField] private float collectingTimer = 4;
    [Space]
    [SerializeField] private BoxCollider2D computerCollider;
    [SerializeField] private GameObject supplySprite;
    [Space]
    [SerializeField] private GameObject collectingDisplay;
    [SerializeField] private Slider collectingslider;
    [Header("Supply Depot Sttings")]
    [SerializeField] private int soldierCount;

    private bool isIn;
    private bool collecting;

    private PlayerController pc;
    private SupplyDepot sd;


    private void Start()
    {
        pc = FindObjectOfType<PlayerController>();
        sd = GetComponentInParent<SupplyDepot>();

        if (sd == null)
        {
            gameObject.AddComponent<SupplyDepot>();
            sd = GetComponentInParent<SupplyDepot>();
        }
        sd.RanSoldiers();
        sd.soldiers = soldierCount;
    }
    private void Update()
    {
        if(isIn && Input.GetKeyDown(KeyCode.E))
        {
            pc.canMove = false;
            isIn = false;
            computerCollider.enabled = false;
            StartCoroutine(Collecting());
        }
        if(collecting)
        {
            collectingDisplay.SetActive(true);
            collectingslider.maxValue = collectingTimer;
            collectingslider.value += Time.deltaTime;

            if(collectingslider.value >= collectingslider.maxValue)
            {
                collecting = false;
                collectingDisplay.SetActive(false);
                collectingslider.value = 0;
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if(player != null)
        {
            isIn = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if(player != null)
        {
            isIn = false;
        }
    }
    private IEnumerator Collecting()
    {
        collecting = true;

        Data data = FindObjectOfType<Data>();

        yield return new WaitForSeconds(collectingTimer);

        supplySprite.SetActive(false);

        if(isRock)
        {
            data.rocks += amount;
        }
        if(isIron)
        {
            data.irons += amount;
        }
        if(isFuel)
        {
            data.fuels += amount;
        }

        pc.canMove = true;
        //start a new timer for it to be replaced or refilled.
        //before being refilled or replaced have a collinder that make sure the player is not nere by.
    }
}
