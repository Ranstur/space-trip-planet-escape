﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainComputer : MonoBehaviour
{
    [SerializeField] private bool Testing;
    [Header("Buttons")]
    [SerializeField] private Button utilityTab;
    [SerializeField] private Button resourceTabButton;
    [SerializeField] private Button defenceTabButton;
    [SerializeField] private Button objectiveTabButton;
    [Header("Sub Buttons")]
    [SerializeField] private Button mainObjectiveButton;
    [SerializeField] private Button secondaryObjectiveButton;
    [Header("UIs")]
    [SerializeField] private GameObject utilityUI;
    [SerializeField] private GameObject backgroundUIPick;
    [SerializeField] private GameObject resourceUI;
    [SerializeField] private GameObject defenceUI;
    [SerializeField] private GameObject objectiveUI;
    [Header("Sub UIs")]
    [SerializeField] private GameObject mainObjectiveUI;
    [SerializeField] private GameObject secondaryObjectiveUI;
    [Header("Texts")]
    [SerializeField] private GameObject cantAffordText;
    [SerializeField] private float affordTextTimer;

    private bool isIn;
    private bool isUsing;
    private bool locked;

    private Animator anim;

    private const string animParamBoolOpen = "Open";

    private void Start()
    {
        anim = GetComponent<Animator>();

        utilityTab.onClick.AddListener(OnUtilityTab);
        resourceTabButton.onClick.AddListener(OnResourceTab);
        defenceTabButton.onClick.AddListener(OnDefenceTab);
        objectiveTabButton.onClick.AddListener(OnObjectiveTab);

        mainObjectiveButton.onClick.AddListener(OnMainObjective);
        secondaryObjectiveButton.onClick.AddListener(OnSecondaryObjective);

        if(!Testing)
        {
            backgroundUIPick.SetActive(false);
            resourceUI.SetActive(false);
            defenceUI.SetActive(false);
            objectiveUI.SetActive(false);
            mainObjectiveUI.SetActive(false);
            secondaryObjectiveUI.SetActive(false);
        }
        else
        {
            Debug.Log($"Testing mode is on! { gameObject.name}");
        }
    }

    private void Update()
    {
        anim.SetBool(animParamBoolOpen, isIn);

        if(Input.GetKeyDown(KeyCode.E) && isIn && !isUsing)
        {
            isUsing = true;
            Open();
        }
        else if(Input.GetKeyDown(KeyCode.E) && isUsing)
        {
            isUsing = false;
            Close();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if (player != null)
        {
            isIn = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if (player != null)
        {
            isIn = false;
        }
    }
    private void Open()
    {
        PlayerController pc = FindObjectOfType<PlayerController>();
        pc.canMove = false;

        backgroundUIPick.SetActive(true);
    }
    private void Close()
    {
        PlayerController pc = FindObjectOfType<PlayerController>();
        pc.canMove = true;

        backgroundUIPick.SetActive(false);
        resourceUI.SetActive(false);
        defenceUI.SetActive(false);
        objectiveUI.SetActive(false);
        mainObjectiveUI.SetActive(false);
        secondaryObjectiveUI.SetActive(false);
    }
    private void OnUtilityTab()
    {
        utilityUI.SetActive(true);
        resourceUI.SetActive(false);
        defenceUI.SetActive(false);
        objectiveUI.SetActive(false);
        mainObjectiveUI.SetActive(false);
        secondaryObjectiveUI.SetActive(false);
    }
    private void OnResourceTab()
    {
        utilityUI.SetActive(false);
        resourceUI.SetActive(true);
        defenceUI.SetActive(false);
        objectiveUI.SetActive(false);
        mainObjectiveUI.SetActive(false);
        secondaryObjectiveUI.SetActive(false);
    }
    private void OnDefenceTab()
    {
        utilityUI.SetActive(false);
        resourceUI.SetActive(false);
        defenceUI.SetActive(true);
        objectiveUI.SetActive(false);
        mainObjectiveUI.SetActive(false);
        secondaryObjectiveUI.SetActive(false);
    }
    private void OnObjectiveTab()
    {
        utilityUI.SetActive(false);
        resourceUI.SetActive(false);
        defenceUI.SetActive(false);
        objectiveUI.SetActive(true);
        mainObjectiveUI.SetActive(true);
        secondaryObjectiveUI.SetActive(false);
    }
    private void OnMainObjective()
    {
        mainObjectiveUI.SetActive(true);
        secondaryObjectiveUI.SetActive(false);
    }
    private void OnSecondaryObjective()
    {
        mainObjectiveUI.SetActive(false);
        secondaryObjectiveUI.SetActive(true);
    }

    public IEnumerator TextDelay()
    {
        if (!locked)
        {
            locked = true;

            cantAffordText.SetActive(true);

            yield return new WaitForSeconds(affordTextTimer);

            cantAffordText.SetActive(false);

            locked = false;
        }
    }
}
