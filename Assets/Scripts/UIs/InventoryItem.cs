﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem : MonoBehaviour
{
    [SerializeField] private Button itemButton;
    [SerializeField] private Button backButton;
    [SerializeField] private Button trashButton;
    [SerializeField] private GameObject itemUI;
    [Header("what it is")]
    public bool weaponPart;
    public bool weapon;
    public bool water;
    public bool food;
    public bool heal;
    [Header("parts")]
    public bool chambers;
    public bool barrels;
    public bool handles;
    public bool armSupports;
    [Header("Chambers")]
    public bool autoChamber;
    public bool chamber;
    public bool semiChamber;
    [Header("Barrels")]
    public bool silencerBarrel;
    public bool barrel;
    public bool barrelRanger;
    [Header("Handles")]
    public bool autoHandle;
    public bool handle;
    public bool semiHandle;
    [Header("Arm-Supports")]
    public bool autoArmSupport;
    public bool recoilArmSupport;
    public bool armSupport;
    [Header("Armor")]
    public bool leather;
    public bool iron;
    public bool steel;
    public bool electric;
    public bool superElectric;

    private bool part;
    private bool armor;
    private bool mod;

    private Inventory inventory;
    private InventoryData inventData;
    private void Start()
    {
        inventory = FindObjectOfType<Inventory>();
        inventData = FindObjectOfType<InventoryData>();

        itemButton.onClick.AddListener(OnItem);
        backButton.onClick.AddListener(OnBack);
        trashButton.onClick.AddListener(OnTrash);

        itemUI.transform.parent = null;
    }
    private void OnItem()
    {  
        if(chamber || semiChamber || autoChamber || handle || semiHandle || autoHandle || barrel || barrelRanger || silencerBarrel || armSupport || autoArmSupport || recoilArmSupport)
        {
            part = true;
        }
        if(leather || iron || steel || electric || superElectric)
        {
            armor = true;
        }

        inventory.locked = true;
        itemUI.SetActive(true);

        if (part)
        {
            inventory.partsLayoutParent.SetActive(false);
            inventory.buttonPanel.SetActive(false);
        }
        if(armor)
        {
            inventory.armorLayoutParent.SetActive(false);
            inventory.buttonPanel.SetActive(false);
        }
        if (mod)
        {

        }

    }
    private void OnBack()
    {
        inventory.locked = false;
        itemUI.SetActive(false);

        if (part)
        {
            inventory.partsLayoutParent.SetActive(true);
            inventory.buttonPanel.SetActive(true);
        }
        if(armor)
        {
            inventory.armorLayoutParent.SetActive(true);
            inventory.buttonPanel.SetActive(true);
        }
        if(mod)
        {

        }
    }
    private void OnTrash()
    {
        inventory.locked = false;

        if (part)
        {
            inventory.partsLayoutParent.SetActive(true);
            inventory.buttonPanel.SetActive(true);

            inventData.totalParts -= 1;

            if(chamber)
            {
                inventData.chamber -= 1;
            }
            if(semiChamber)
            {
                inventData.semiChamber -= 1;
            }
            if(autoChamber)
            {
                inventData.autoChamber -= 1;
            }
            if(barrel)
            {
                inventData.barrel -= 1;
            }
            if(barrelRanger)
            {
                inventData.barrelRanger -= 1;
            }
            if(silencerBarrel)
            {
                inventData.silencerBarrel -= 1;
            }
            if(handle)
            {
                inventData.handle -= 1;
            }
            if(semiHandle)
            {
                inventData.semiHandle -= 1;
            }
            if(autoHandle)
            {
                inventData.autoHandle -= 1;
            }
            if(armSupport)
            {
                inventData.armSupport -= 1;
            }
            if(autoArmSupport)
            {
                inventData.autoArmSupport -= 1;
            }
            if(recoilArmSupport)
            {
                inventData.recoilArmSupport -= 1;
            }
        }
        if(armor)
        {
            inventory.armorLayoutParent.SetActive(true);
            inventory.buttonPanel.SetActive(true);

            inventData.totalArmor -= 1;

            if(leather)
            {
                inventData.leather -= 1;
            }
            if(iron)
            {
                inventData.iron -= 1;
            }
            if(steel)
            {
                inventData.steel -= 1;
            }
            if(electric)
            {
                inventData.electric -= 1;
            }
            if(superElectric)
            {
                inventData.superElectric -= 1;
            }
        }
        if(mod)
        {

        }

        Destroy(itemUI);
        Destroy(gameObject);
    }
}
