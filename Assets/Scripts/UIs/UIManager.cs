﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    public int rocks;
    public int irons;
    public int fuels;
    [Space]
    [SerializeField] private TextMeshProUGUI rockText;
    [SerializeField] private TextMeshProUGUI ironText;
    [SerializeField] private TextMeshProUGUI fuelText;
    [Header("other ones")]
    [SerializeField] private TextMeshProUGUI rockText2;
    [SerializeField] private TextMeshProUGUI ironText2;
    [SerializeField] private TextMeshProUGUI fuelText2;

    private Data data;
    private void FixedUpdate()
    {
        data = FindObjectOfType<Data>();

        rocks = data.rocks;
        irons = data.irons;
        fuels = data.fuels;

        SetTexts();
    }
    public void SetTexts()
    {
        SetRocks(rocks);
        SetIrons(irons);
        SetFuels(fuels);
    }
    public void SetRocks(int amount)
    {
        rockText.text = ($"{amount}");
        rockText2.text = ($"{amount}");
    }
    public void SetIrons(int amount)
    {
        ironText.text = ($"{amount}");
        ironText2.text = ($"{amount}");
    }
    public void SetFuels(int amount)
    {
        fuelText.text = ($"{amount}");
        fuelText2.text = ($"{amount}");
    }
}
