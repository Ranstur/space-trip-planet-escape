﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenuUI : MonoBehaviour
{
    [Header("Buttons")]
    [SerializeField] private Button useAutoSaveButton;
    [SerializeField] private Button backButton;
    [Header("Triggers")]
    [SerializeField] private GameObject checkMark;
    [SerializeField] private InputField autoSaveInput;

    private int autoSaveIntervals;

    private SaveManager dataBanks;
    private void Start()
    {
        useAutoSaveButton.onClick.AddListener(OnUseAutoSave);
        backButton.onClick.AddListener(OnBack);
    }
    private void OnBack()
    {
        StartCoroutine(AutoSave());
    }
    private void OnUseAutoSave()
    {
        if (!checkMark.activeSelf)
        {
            checkMark.SetActive(true);
        }
        else
        {
            checkMark.SetActive(false);
            StopCoroutine(AutoSave());
        }
    }
    private void AutoSaveInput()
    {
        //have a player input to set the "autoSaveIntervals"
        //Start the coroutine
    }
    IEnumerator AutoSave()
    {
        yield return new WaitForSeconds(autoSaveIntervals);

        dataBanks = FindObjectOfType<SaveManager>();
        dataBanks.Save();

        StartCoroutine(AutoSave());
    }
}
