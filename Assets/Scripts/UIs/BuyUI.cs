﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuyUI : MonoBehaviour
{
    [Header("Price")]
    [SerializeField] [Range(0, 100000)] private int rockPrice;
    [SerializeField] [Range(0, 100000)] private int ironPrice;
    [SerializeField] [Range(0, 100000)] private int fuelPrice;
    [Space]
    [SerializeField] private Button buyButton;
    [SerializeField] private GameObject boughtCover;
    [SerializeField] private GameObject boughtObject;
    [Header("Texts")]
    [SerializeField] private TextMeshProUGUI rockText;
    [SerializeField] private TextMeshProUGUI ironText;
    [SerializeField] private TextMeshProUGUI fuelText;
    [Header("Resources")]
    [SerializeField] private bool assemblyBench;
    [SerializeField] private bool moddingTable;
    [SerializeField] private bool upgradeBench;
    [SerializeField] private bool buildingBench;
    [SerializeField] private bool armorBench;
    [SerializeField] private bool recycler;
    [SerializeField] private bool rockBreaker;
    [SerializeField] private bool ironMine;
    [SerializeField] private bool fuelExtractor;
    [Header("Defences")]
    [Header("Second Objective Getting A Weapon")]
    [SerializeField] private GameObject cross;

    //bought bool.
    private bool boughtAssemblyBench;
    private bool boughtModdingTable;
    private bool boughtUpgadeBench;
    private bool boughtBuildingBench;
    private bool boughtRecycler;
    private bool boughtArmorBench;
    private bool boughtRockBreaker;
    private bool boughtIronMine;
    private bool boughtFuelExtractor;

    private Data data;
    private LevelData levelData;
    private SecondaryObjectiveGettingAWeapon secondaryObjective;

    private void Start()
    {
        data = FindObjectOfType<Data>();
        levelData = FindObjectOfType<LevelData>();
        secondaryObjective = FindObjectOfType<SecondaryObjectiveGettingAWeapon>();

        rockText.text = $"{rockPrice}";
        ironText.text = $"{ironPrice}";
        fuelText.text = $"{fuelPrice}";

        boughtObject.SetActive(false);

        boughtAssemblyBench = levelData.boughtAssemblyBench;
        boughtModdingTable = levelData.boughtModdingTable;
        boughtUpgadeBench = levelData.boughtUpgradeBench;
        boughtBuildingBench = levelData.boughtBuildingBench;
        boughtRecycler = levelData.boughtRecycler;
        boughtArmorBench = levelData.boughtArmorBench;
        boughtRockBreaker = levelData.boughtRockBreaker;
        boughtIronMine = levelData.boughtIronMine;
        boughtFuelExtractor = levelData.boughtFuelExtractor;

        buyButton.onClick.AddListener(OnBuy);
    }
    private void FixedUpdate()
    {
        if(boughtAssemblyBench && assemblyBench)
        {
            boughtObject.SetActive(true);
            cross.SetActive(true);
            Disabled();
        }
        if(boughtModdingTable && moddingTable)
        {
            boughtObject.SetActive(true);
            Disabled();
        }
        if(boughtUpgadeBench && upgradeBench)
        {
            boughtObject.SetActive(true);
            Disabled();
        }
        if(boughtBuildingBench && buildingBench)
        {
            boughtObject.SetActive(true);
            Disabled();
        }
        if(boughtRecycler && recycler)
        {
            boughtObject.SetActive(true);
            Disabled();
        }
        if(boughtArmorBench && armorBench)
        {
            boughtObject.SetActive(true);
            Disabled();
        }
        if(boughtRockBreaker && rockBreaker)
        {
            boughtObject.SetActive(true);
            Disabled();
        }
        if(boughtIronMine && ironMine)
        {
            boughtObject.SetActive(true);
            Disabled();
        }
        if(boughtFuelExtractor && fuelExtractor)
        {
            boughtObject.SetActive(true);
            Disabled();
        }
    }
    private void Disabled()
    {
        buyButton.enabled = false;
        boughtCover.SetActive(true);
    }
    private void OnBuy()
    {
        if(data.rocks >= rockPrice && data.irons >= ironPrice && data.fuels >= fuelPrice)
        {
            if(assemblyBench)
            {
                boughtAssemblyBench = true;
                levelData.boughtAssemblyBench = true;
                cross.SetActive(true);
                boughtObject.SetActive(true);
                Disabled();
            }
            if(moddingTable)
            {
                boughtModdingTable = true;
                levelData.boughtModdingTable = true;
                boughtObject.SetActive(true);
                Disabled();
            }
            if(upgradeBench)
            {
                boughtUpgadeBench = true;
                levelData.boughtUpgradeBench = true;
                boughtObject.SetActive(true);
                Disabled();
            }
            if(buildingBench)
            {
                boughtBuildingBench = true;
                levelData.boughtBuildingBench = true;
                boughtObject.SetActive(true);
                Disabled();
            }
            if(recycler)
            {
                boughtRecycler = true;
                levelData.boughtRecycler = true;
                boughtObject.SetActive(true);
                Disabled();
            }
            if(armorBench)
            {
                boughtArmorBench = true;
                levelData.boughtArmorBench = true;
                boughtObject.SetActive(true);
                Disabled();
            }
            if(rockBreaker)
            {
                boughtRockBreaker = true;
                levelData.boughtRockBreaker = true;
                boughtObject.SetActive(true);
                Disabled();
            }
            if(ironMine)
            {
                boughtIronMine = true;
                levelData.boughtIronMine = true;
                boughtObject.SetActive(true);
                Disabled();
            }
            if(fuelExtractor)
            {
                boughtFuelExtractor = true;
                levelData.boughtFuelExtractor = true;
                boughtObject.SetActive(true);
                Disabled();
            }

            data.rocks -= rockPrice;
            data.irons -= ironPrice;
            data.fuels -= fuelPrice;
        }
        else
        {
            MainComputer computer = FindObjectOfType<MainComputer>();
            StartCoroutine(computer.TextDelay());
        }
    }
}
