﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    [SerializeField] private Button partsButton;
    [SerializeField] private Button armorButton;
    [SerializeField] private Button modsButton;
    [Space]
    [SerializeField] private GameObject inventory;
    [Header("other scripts")]
    [SerializeField] private GameObject playerHub;
    [SerializeField] public GameObject partsLayoutParent;
    [SerializeField] public GameObject armorLayoutParent;
    [SerializeField] public GameObject modsLayoutParent;
    [SerializeField] public GameObject buttonPanel;
    [SerializeField] private GameObject infoLayoutParent;

    //you can fit 18 items in the inventory at once.

    public bool locked;

    private bool inUse;
    //private GameObject currentItem;

    private void Start()
    {
        inventory.SetActive(false);

        partsButton.onClick.AddListener(OnWeaponParts);
        armorButton.onClick.AddListener(OnArmor);
        modsButton.onClick.AddListener(OnMods);
    }
    private void Update()
    {
        PlayerController PC = FindObjectOfType<PlayerController>();

        if(Input.GetKeyDown(KeyCode.Tab) && !inUse)
        {
            inUse = true;

            PC.canMove = false;

            playerHub.SetActive(false);
            inventory.SetActive(true);
        }
        else if(Input.GetKeyDown(KeyCode.Tab) && inUse && !locked)
        {
            inUse = false;

            PC.canMove = true;

            playerHub.SetActive(true);
            inventory.SetActive(false);
        }
    }
    public void AddToInventory(GameObject item)
    {
        GameObject inventItem = Instantiate(item); 
        inventItem.transform.parent = partsLayoutParent.transform;

        //currentItem = inventItem;
    }
    public void AddArmor(GameObject item)
    {
        GameObject inventItem = Instantiate(item);
        inventItem.transform.parent = armorLayoutParent.transform;
    }
    public void AddMods(GameObject item)
    {
        GameObject inventItem = Instantiate(item);
        inventItem.transform.parent = modsLayoutParent.transform;
    }
    private void OnWeaponParts()
    {
        partsLayoutParent.SetActive(true);
        armorLayoutParent.SetActive(false);
        modsLayoutParent.SetActive(false);
    }
    private void OnArmor()
    {
        armorLayoutParent.SetActive(true);
        partsLayoutParent.SetActive(false);
        modsLayoutParent.SetActive(false);
    }
    private void OnMods()
    {
        modsLayoutParent.SetActive(true);
        partsLayoutParent.SetActive(false);
        armorLayoutParent.SetActive(false);
    }
    //public void AddInfoInventory(GameObject itemInfo)
    //{
    //    GameObject inventItemInfo = Instantiate(itemInfo);
    //    inventItemInfo.transform.parent = infoLayoutParent.transform;
    //
    //    InventoryItem current =  currentItem.GetComponent<InventoryItem>();
    //    current.itemUI = inventItemInfo;
    //}
}
