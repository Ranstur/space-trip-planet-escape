﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuUI : MonoBehaviour
{
    [SerializeField] private Button mainMenuButton;
    [SerializeField] private Button settingsButton;
    [SerializeField] private Button saveButton;
    [SerializeField] private Button quitButton;
    [SerializeField] private Button backButton;
    [Space]
    [SerializeField] private GameObject mainMenuUI;
    [SerializeField] private GameObject settingsMenuUI;

    private bool isPaused;
    private SaveManager saveManager;

    private void Start()
    {
        saveManager = FindObjectOfType<SaveManager>();

        mainMenuButton.onClick.AddListener(OnMainMenu);
        settingsButton.onClick.AddListener(OnSettings);
        saveButton.onClick.AddListener(OnSave);
        quitButton.onClick.AddListener(OnQuit);

        backButton.onClick.AddListener(OnBack);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!isPaused)
            {
                Pause();
            }
            else Resume();
        }
    }
    private void Pause()
    {
        isPaused = true;
        Time.timeScale = 0f;
    }
    private void Resume()
    {
        isPaused = false;
        Time.timeScale = 1f;
    }
    private void OnMainMenu()
    {
        Time.timeScale = 1f;
        isPaused = false;
        SceneManager.LoadScene(0);
    }
    private void OnSettings()
    {
        settingsMenuUI.SetActive(true);
        mainMenuUI.SetActive(false);
    }
    private void OnSave()
    {
        saveManager.Save();
    }
    private void OnQuit()
    {
        Application.Quit();
        Debug.Log("Quitting game.");
    }
    private void OnBack()
    {
        mainMenuUI.SetActive(true);
        settingsMenuUI.SetActive(false);
    }
}
