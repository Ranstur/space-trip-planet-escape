﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UtilityTabUI : MonoBehaviour
{
    [Header("Generator")]
    [SerializeField] private Image indacator;
    [SerializeField] private Material green;
    [SerializeField] private Material red;

    private Generator generator;

    private void Start()
    {
        generator = FindObjectOfType<Generator>();
    }
    private void Update()
    {
        if (generator.makingPower)
        {
            indacator.material = green;
        }
        else indacator.material = red;
    }
}
