﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    [SerializeField] private GameObject cheatUI;

    private bool isIn;
    private bool inUse;

    private void Update()
    {
        PlayerController pc = FindObjectOfType<PlayerController>();

        if(Input.GetKeyDown(KeyCode.E) && isIn && !inUse)
        {
            inUse = true;
            pc.canMove = false;
            cheatUI.SetActive(true);
        }
        else if(Input.GetKeyDown(KeyCode.E) && inUse)
        {
            inUse = false;
            pc.canMove = true;
            cheatUI.SetActive(false);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if(player != null)
        {
            isIn = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        if(player != null)
        {
            isIn = false;
        }
    }
}
