﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySpawner : MonoBehaviour
{
    public int maxSlots = 18;
    public int totalParts;
    public int totalArmor;
    public int totalMods;

    [Header("Weapon Parts")]
    public int chamber;
    public int semiChamber;
    public int autoChamber;
    [Space]
    public int handle;
    public int semiHandle;
    public int autoHandle;
    [Space]
    public int barrel;
    public int barrelRanger;
    public int silencerBarrel;
    [Space]
    public int armSupport;
    public int autoArmSupport;
    public int recoilArmSupport;
    [Header("Armor")]
    public int leather;
    public int iron;
    public int steel;
    public int electric;
    public int superElectric;
    [Header("Mods")]
    [Header("Weapon Parts Prefabs")]
    [SerializeField] private GameObject chamberPrefab;
    [SerializeField] private GameObject semiChamberPrefab;
    [SerializeField] private GameObject autoChamberPrefab;
    [Space]
    [SerializeField] private GameObject handlePrefab;
    [SerializeField] private GameObject semiHandlePrefab;
    [SerializeField] private GameObject autoHandlePrefab;
    [Space]
    [SerializeField] private GameObject barrelPrefab;
    [SerializeField] private GameObject barrelRangerPrefab;
    [SerializeField] private GameObject silencerBarrelPrefab;
    [Space]
    [SerializeField] private GameObject armSupportPrefab;
    [SerializeField] private GameObject autoArmSupportPrefab;
    [SerializeField] private GameObject recoilArmSupportPrefab;
    [Header("Armor Prefabs")]
    [SerializeField] private GameObject leatherPrefab;
    [SerializeField] private GameObject ironPrefab;
    [SerializeField] private GameObject steelPrefab;
    [SerializeField] private GameObject electricPrefab;
    [SerializeField] private GameObject superElectricPrefab;
    [Header("Mods Prefabs")]

    private InventoryData inventData;
    private Inventory inventory;

    private void Start()
    {
        inventData = FindObjectOfType<InventoryData>();
        inventory = FindObjectOfType<Inventory>();

        chamber = inventData.chamber;
        semiChamber = inventData.semiChamber;
        autoChamber = inventData.autoChamber;
        handle = inventData.handle;
        semiHandle = inventData.semiHandle;
        autoHandle = inventData.autoHandle;
        barrel = inventData.barrel;
        barrelRanger = inventData.barrelRanger;
        silencerBarrel = inventData.silencerBarrel;
        armSupport = inventData.armSupport;
        autoArmSupport = inventData.autoArmSupport;
        recoilArmSupport = inventData.recoilArmSupport;

        leather = inventData.leather;
        //armor here
    }
    private void Update()
    {
        totalParts = inventData.totalParts;
        totalArmor = inventData.totalArmor;
        totalMods = inventData.totalMods;

        if(chamber > 0)
        {
            inventory.AddToInventory(chamberPrefab);

            chamber -= 1;
        }
        if(semiChamber > 0)
        {
            inventory.AddToInventory(semiChamberPrefab);

            semiChamber -= 1;
        }
        if(autoChamber > 0)
        {
            inventory.AddToInventory(autoChamberPrefab);

            autoChamber -= 1;
        }
        if(handle > 0)
        {
            inventory.AddToInventory(handlePrefab);

            handle -= 1;
        }
        if(semiHandle > 0)
        {
            inventory.AddToInventory(semiHandlePrefab);

            semiHandle -= 1;
        }
        if(autoHandle > 0)
        {
            inventory.AddToInventory(autoHandlePrefab);

            autoHandle -= 1;
        }
        if(barrel > 0)
        {
            inventory.AddToInventory(barrelPrefab);

            barrel -= 1;
        }
        if(barrelRanger > 0)
        {
            inventory.AddToInventory(barrelRangerPrefab);

            barrelRanger -= 1;
        }
        if(silencerBarrel > 0)
        {
            inventory.AddToInventory(silencerBarrelPrefab);

            silencerBarrel -= 1;
        }
        if(armSupport > 0)
        {
            inventory.AddToInventory(armSupportPrefab);

            armSupport -= 1;
        }
        if(autoArmSupport > 0)
        {
            inventory.AddToInventory(autoArmSupportPrefab);

            autoArmSupport -= 1;
        }
        if(recoilArmSupport > 0)
        {
            inventory.AddToInventory(recoilArmSupportPrefab);

            recoilArmSupport -= 1;
        }
        if(leather > 0)
        {
            inventory.AddArmor(leatherPrefab);

            leather -= 1;
        }
        if(iron > 0)
        {
            inventory.AddArmor(ironPrefab);

            iron -= 1;
        }
        if(steel > 0)
        {
            inventory.AddArmor(steelPrefab);

            steel -= 1;
        }
        if(electric > 0)
        {
            inventory.AddArmor(electricPrefab);

            electric -= 1;
        }
        if(superElectric > 0)
        {
            inventory.AddArmor(superElectricPrefab);

            superElectric -= 1;
        }
    }
}
