﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float damage;
    [SerializeField] private float decay;

    private void Update()
    {
        transform.Translate(-speed , 0 ,0);
        Destroy(gameObject, decay);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Player player = collision.GetComponent<Player>();
        Enemy enemy = collision.GetComponent<Enemy>();

        if(player != null)
        {
            player.TakeDamage(damage);
        }
        if(enemy != null)
        {
            enemy.TakeDamage(damage);
        }
    }
}
