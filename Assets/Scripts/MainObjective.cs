﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainObjective : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI rockText;
    [SerializeField] private TextMeshProUGUI ironText;
    [SerializeField] private TextMeshProUGUI fuelText;
    [Space]
    [SerializeField] private GameObject panel1;
    [Space]
    [SerializeField] private Button cleanUpButton;
    [Space]
    [SerializeField] private int cleanedRocks;
    [SerializeField] private int cleanedIron;
    [SerializeField] private int cleanedFuel;
    [Space]
    [SerializeField] private GameObject panel2;
    [SerializeField] private Button buy2Button;
    [Space]
    [SerializeField] private int rockCost;
    [SerializeField] private int ironCost;
    [SerializeField] private int fuelCost;
    [Space]
    [SerializeField] private GameObject panel3;
    [SerializeField] private Button buy3Button;
    [Space]
    [SerializeField] private int rock3Cost;
    [SerializeField] private int iron3Cost;
    [SerializeField] private int fuel3Cost;
    [Space]
    [SerializeField] private GameObject panel4;
    [SerializeField] private Button buy4Button;
    [Space]
    [SerializeField] private int rock4Cost;
    [SerializeField] private int iron4Cost;
    [SerializeField] private int fuel4Cost;
    [Space]
    [SerializeField] private GameObject panel5;
    [SerializeField] private Button buy5Button;
    [Space]
    [SerializeField] private int rock5Cost;
    [SerializeField] private int iron5Cost;
    [SerializeField] private int fuel5Cost;
    [Space]
    [SerializeField] private GameObject panel6;
    [SerializeField] private Button buy6Button;
    [Space]
    [SerializeField] private int rock6Cost;
    [SerializeField] private int iron6Cost;
    [SerializeField] private int fuel6Cost;
    [Space]
    [SerializeField] private GameObject panel7;
    [SerializeField] private Button buy7Button;
    [Space]
    [SerializeField] private int rock7Cost;
    [SerializeField] private int iron7Cost;
    [SerializeField] private int fuel7Cost;
    [Space]
    [SerializeField] private GameObject panel8;
    [SerializeField] private Button buy8Button;
    [Space]
    [SerializeField] private int rock8Cost;
    [SerializeField] private int iron8Cost;
    [SerializeField] private int fuel8Cost;

    private Data data;

    private bool firstObjcetiveDone;
    private bool secondObjectiveDone;
    private bool threeObjectiveDone;
    private bool fourObjectiveDone;
    private bool fiveOjectiveDone;
    private bool sixOjectiveDone;
    private bool sevenOjectiveDone;
    private bool eightOjectiveDone;

    private void Start()
    {
        data = FindObjectOfType<Data>();
        firstObjcetiveDone = data.mainFirstObjectiveDone;
        secondObjectiveDone = data.mainSecondObjectiveDone;
        threeObjectiveDone = data.mainThreeObjectiveDone;
        fourObjectiveDone = data.mainFourObjectiveDone;
        fiveOjectiveDone = data.mainFiveObjectiveDone;
        sixOjectiveDone = data.mainSixObjectiveDone;
        sevenOjectiveDone = data.mainSevenObjectiveDone;
        eightOjectiveDone = data.mainEightObjectiveDone;

        cleanUpButton.onClick.AddListener(OnCleanUp);
        buy2Button.onClick.AddListener(OnBuy1);
        buy3Button.onClick.AddListener(OnBuy2);
        buy4Button.onClick.AddListener(OnBuy3);
        buy5Button.onClick.AddListener(OnBuy4);
        buy6Button.onClick.AddListener(OnBuy5);
        buy7Button.onClick.AddListener(OnBuy6);
        buy8Button.onClick.AddListener(OnBuy7);
    }
    private void Update()
    {
        secondObjectiveDone = data.mainSecondObjectiveDone;
        threeObjectiveDone = data.mainThreeObjectiveDone;
        fourObjectiveDone = data.mainFourObjectiveDone;
        fiveOjectiveDone = data.mainFiveObjectiveDone;
        sixOjectiveDone = data.mainSixObjectiveDone;
        sevenOjectiveDone = data.mainSevenObjectiveDone;
        eightOjectiveDone = data.mainEightObjectiveDone;

        if(!firstObjcetiveDone)
        {
            panel1.SetActive(true);
            panel2.SetActive(false);

            rockText.enabled = false;
            ironText.enabled = false;
            fuelText.enabled = false;
        }
        if(firstObjcetiveDone)
        {
            panel1.SetActive(false);
            panel2.SetActive(true);

            rockText.enabled = true;
            ironText.enabled = true;
            fuelText.enabled = true;

            rockText.text = $"{rockCost}";
            ironText.text = $"{ironCost}";
            fuelText.text = $"{fuelCost}";
        }
        if(secondObjectiveDone)
        {
            panel2.SetActive(false);
            panel3.SetActive(true);

            rockText.text = $"{rock3Cost}";
            ironText.text = $"{iron3Cost}";
            fuelText.text = $"{fuel3Cost}";
        }
        if(threeObjectiveDone)
        {
            panel3.SetActive(false);
            panel4.SetActive(true);

            rockText.text = $"{rock4Cost}";
            ironText.text = $"{iron4Cost}";
            fuelText.text = $"{fuel4Cost}";
        }
        if(fourObjectiveDone)
        {
            panel4.SetActive(false);
            panel5.SetActive(true);

            rockText.text = $"{rock5Cost}";
            ironText.text = $"{iron5Cost}";
            fuelText.text = $"{fuel5Cost}";
        }
        if(fiveOjectiveDone)
        {
            panel5.SetActive(false);
            panel6.SetActive(true);

            rockText.text = $"{rock6Cost}";
            ironText.text = $"{iron6Cost}";
            fuelText.text = $"{fuel6Cost}";
        }
        if(sixOjectiveDone)
        {
            panel6.SetActive(false);
            panel7.SetActive(true);

            rockText.text = $"{rock7Cost}";
            ironText.text = $"{iron7Cost}";
            fuelText.text = $"{fuel7Cost}";
        }
        if(sevenOjectiveDone)
        {
            panel7.SetActive(false);
            panel8.SetActive(true);

            rockText.text = $"{rock8Cost}";
            ironText.text = $"{iron8Cost}";
            fuelText.text = $"{fuel8Cost}";
        }
        if(eightOjectiveDone)
        {
            panel8.SetActive(false);

            rockText.enabled = false;
            ironText.enabled = false;
            fuelText.enabled = false;
        }
    }
    private void OnCleanUp()
    {
        data.mainFirstObjectiveDone = true;
        firstObjcetiveDone = true;

        data.rocks += cleanedRocks;
        data.irons += cleanedIron;
        data.fuels += cleanedFuel;
    }
    private void OnBuy1()
    {
        if(data.rocks >= rockCost && data.irons >= ironCost && data.fuels >= fuelCost)
        {
            data.rocks -= rockCost;
            data.irons -= ironCost;
            data.fuels -= fuelCost;
            data.mainSecondObjectiveDone = true;
        }
    }
    private void OnBuy2()
    {
        if (data.rocks >= rock3Cost && data.irons >= iron3Cost && data.fuels >= fuel3Cost)
        {
            data.rocks -= rock3Cost;
            data.irons -= iron3Cost;
            data.fuels -= fuel3Cost;
            data.mainThreeObjectiveDone = true;
        }
    }
    private void OnBuy3()
    {
        if (data.rocks >= rock4Cost && data.irons >= iron4Cost && data.fuels >= fuel4Cost)
        {
            data.rocks -= rock4Cost;
            data.irons -= iron4Cost;
            data.fuels -= fuel4Cost;
            data.mainFourObjectiveDone = true;
        }
    }
    private void OnBuy4()
    {
        if (data.rocks >= rock5Cost && data.irons >= iron5Cost && data.fuels >= fuel5Cost)
        {
            data.rocks -= rock5Cost;
            data.irons -= iron5Cost;
            data.fuels -= fuel5Cost;
            data.mainFiveObjectiveDone = true;
        }
    }
    private void OnBuy5()
    {
        if (data.rocks >= rock6Cost && data.irons >= iron6Cost && data.fuels >= fuel6Cost)
        {
            data.rocks -= rock6Cost;
            data.irons -= iron6Cost;
            data.fuels -= fuel6Cost;
            data.mainSixObjectiveDone = true;
        }
    }
    private void OnBuy6()
    {
        if (data.rocks >= rock7Cost && data.irons >= iron7Cost && data.fuels >= fuel7Cost)
        {
            data.rocks -= rock7Cost;
            data.irons -= iron7Cost;
            data.fuels -= fuel7Cost;
            data.mainSevenObjectiveDone = true;
        }
    }
    private void OnBuy7()
    {
        if (data.rocks >= rock8Cost && data.irons >= iron8Cost && data.fuels >= fuel8Cost)
        {
            data.rocks -= rock8Cost;
            data.irons -= iron8Cost;
            data.fuels -= fuel8Cost;
            data.mainEightObjectiveDone = true;
        }
    }
}
