﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class SunCycles : MonoBehaviour
{
    [SerializeField] private Light2D sun;
    public float sunLightData;
    public bool sunRising;

    private Data dataBanks;

    void Start()
    {
        dataBanks = FindObjectOfType<Data>();

        sunLightData = dataBanks.dayTimeLighting;
        sunRising = dataBanks.sunRising;

        sun.intensity = sunLightData;

        if (!sunRising)
        {
            StartCoroutine(SunSet());
        }
        else
        {
            StartCoroutine(SunRise());
        }
    }
    private void Update()
    {
        sunLightData = sun.intensity;
        dataBanks.dayTimeLighting = sunLightData;
    }
    IEnumerator SunSet()
    {
        //dataBanks.sunRising = false;

        while(sun.intensity > 0.3f)
        {
            sun.intensity -= 0.001f;
            yield return new WaitForSeconds(0.1f);
        }
        StartCoroutine(SunRise());
        StopCoroutine(SunSet());
    }
    IEnumerator SunRise()
    {

        dataBanks.sunRising = true;

        while(sun.intensity < 1.3f)
        {
            sun.intensity += 0.001f;
            yield return new WaitForSeconds(0.1f);
        }
        StartCoroutine(SunSet());
        StopCoroutine(SunRise());
    }
}
