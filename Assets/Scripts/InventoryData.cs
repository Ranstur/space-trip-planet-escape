﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryData : MonoBehaviour
{
    public int totalParts;
    public int totalArmor;
    public int totalMods;

    [Header("weapon parts")]
    public int chamber;
    public int semiChamber;
    public int autoChamber;

    public int handle;
    public int semiHandle;
    public int autoHandle;

    public int barrel;
    public int barrelRanger;
    public int silencerBarrel;

    public int armSupport;
    public int autoArmSupport;
    public int recoilArmSupport;

    public int leather;
    public int iron;
    public int steel;
    public int electric;
    public int superElectric;
    
    //mods here

}
