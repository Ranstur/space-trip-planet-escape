﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatCodes : MonoBehaviour
{
    [SerializeField] private bool isTesting;

    private bool locked;

    private Data data;

    private void Start()
    {
        data = FindObjectOfType<Data>();
    }
    private void Update()
    {
        if (isTesting)
        {
            if (!locked)
            {
                locked = true;

                Debug.Log($"the Cheat Codes are On! the gameobject that have it is {gameObject.name}");
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Keypad1))
            {
                data.rocks += 100;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Keypad2))
            {
                data.irons += 100;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Keypad3))
            {
                data.fuels += 100;
            }
            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Keypad0))
            {
                data.rocks += 100;
                data.irons += 100;
                data.fuels += 100;
            }
        }
        else
        {
            locked = false;
        }
    }
}
