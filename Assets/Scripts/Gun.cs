﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [Header("weapon class")]
    public bool pistol;
    public bool assaultRifle;
    public bool sniper;
    [Header("parts")]
    public bool chamber;
    public bool autoChamber;
    public bool semiChamber;
    [Space]
    public bool barrel;
    public bool barrelRanger;
    public bool silencerBarrel;
    [Space]
    public bool handle;
    public bool autoHandle;
    public bool semiHandle;
    [Space]
    public bool armSupport;
    public bool autoArmSupport;
    public bool recoilArmSupport;
    [Header("References")]
    [SerializeField] private GameObject chamberSlot;
    [SerializeField] private GameObject barrelSlot;
    [SerializeField] private GameObject handleSlot;
    [SerializeField] private GameObject armSupportSlot;
    [Header("References Parts")]
    [SerializeField] private GameObject chamberP;
    [SerializeField] private GameObject autoChamberP;
    [SerializeField] private GameObject semiChamberP;
    [Space]
    [SerializeField] private GameObject barrelP;
    [SerializeField] private GameObject barrelRangerP;
    [SerializeField] private GameObject silencerBarrelP;
    [Space]
    [SerializeField] private GameObject handleP;
    [SerializeField] private GameObject autoHandleP;
    [SerializeField] private GameObject semiHandleP;
    [Space]
    [SerializeField] private GameObject armSupportP;
    [SerializeField] private GameObject autoArmSupportP;
    [SerializeField] private GameObject recoilArmSupportP;

    private bool locked;

    private void Update()
    {
        if (!locked)
        {
            locked = true;

            if (chamber || autoChamber || semiChamber)
            {
                GameObject chamberS;

                if (chamber)
                {
                    chamberS = Instantiate(chamberP);

                    chamberS.transform.position = chamberSlot.transform.position;
                    chamberS.transform.rotation = chamberSlot.transform.rotation;
                    chamberS.transform.parent = transform;
                }
                if (autoChamber)
                {
                    chamberS = Instantiate(autoChamberP);

                    chamberS.transform.position = chamberSlot.transform.position;
                    chamberS.transform.rotation = chamberSlot.transform.rotation;
                    chamberS.transform.parent = transform;
                }
                if (semiChamber)
                {
                    chamberS = Instantiate(semiChamberP);

                    chamberS.transform.position = chamberSlot.transform.position;
                    chamberS.transform.rotation = chamberSlot.transform.rotation;
                    chamberS.transform.parent = transform;
                }
            }
            if (barrel || barrelRanger || silencerBarrel)
            {
                GameObject barrelS;

                if (barrel)
                {
                    barrelS = Instantiate(barrelP);

                    barrelS.transform.position = barrelSlot.transform.position;
                    barrelS.transform.rotation = barrelSlot.transform.rotation;
                    barrelS.transform.parent = transform;
                }
                if(barrelRanger)
                {
                    barrelS = Instantiate(barrelRangerP);

                    barrelS.transform.position = barrelSlot.transform.position;
                    barrelS.transform.rotation = barrelSlot.transform.rotation;
                    barrelS.transform.parent = transform;
                }
                if(silencerBarrel)
                {
                    barrelS = Instantiate(silencerBarrelP);

                    barrelS.transform.position = barrelSlot.transform.position;
                    barrelS.transform.rotation = barrelSlot.transform.rotation;
                    barrelS.transform.parent = transform;
                }
            }
            if(handle || autoHandle || semiHandle)
            {
                GameObject handleS;

                if(handle)
                {
                    handleS = Instantiate(handleP);
                    
                    handleS.transform.position = handleSlot.transform.position;
                    handleS.transform.rotation = handleSlot.transform.rotation;
                    handleS.transform.parent = transform;
                }
                if(autoHandle)
                {
                    handleS = Instantiate(autoHandleP);

                    handleS.transform.position = handleSlot.transform.position;
                    handleS.transform.rotation = handleSlot.transform.rotation;
                    handleS.transform.parent = transform;
                }
                if(semiHandle)
                {
                    handleS = Instantiate(semiHandleP);

                    handleS.transform.position = handleSlot.transform.position;
                    handleS.transform.rotation = handleSlot.transform.rotation;
                    handleS.transform.parent = transform;
                }
            }
            if(assaultRifle || sniper)
            {
                if(armSupport || autoArmSupport || recoilArmSupport)
                {
                    GameObject armSupportS;

                    if(armSupport)
                    {
                        armSupportS = Instantiate(armSupportP);
                        
                        armSupportS.transform.position = armSupportSlot.transform.position;
                        armSupportS.transform.rotation = armSupportSlot.transform.rotation;
                        armSupportS.transform.parent = transform;
                    }
                    if(autoArmSupport)
                    {
                        armSupportS = Instantiate(autoArmSupportP);

                        armSupportS.transform.position = armSupportSlot.transform.position;
                        armSupportS.transform.rotation = armSupportSlot.transform.rotation;
                        armSupportS.transform.parent = transform;
                    }
                    if(recoilArmSupport)
                    {
                        armSupportS = Instantiate(recoilArmSupportP);

                        armSupportS.transform.position = armSupportSlot.transform.position;
                        armSupportS.transform.rotation = armSupportSlot.transform.rotation;
                        armSupportS.transform.parent = transform;
                    }
                }
            }
        }
    }
}
