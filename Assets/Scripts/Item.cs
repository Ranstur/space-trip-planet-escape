﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    [SerializeField] private Button pickupButton;
    [Space]
    [SerializeField] private bool chamber;
    [SerializeField] private bool semiChamber;
    [SerializeField] private bool autoChamber;
    [SerializeField] private bool handle;
    [SerializeField] private bool semiHandle;
    [SerializeField] private bool autoHandle;
    [SerializeField] private bool barrel;
    [SerializeField] private bool barrelRanger;
    [SerializeField] private bool silencerBarrel;
    [SerializeField] private bool armSupport;
    [SerializeField] private bool autoArmSupport;
    [SerializeField] private bool recoilArmSupport;
    [Space]
    [SerializeField] private bool leather;
    [SerializeField] private bool iron;
    [SerializeField] private bool steel;
    [SerializeField] private bool electric;
    [SerializeField] private bool superElectric;
    //mods here

    private InventoryData inventData;
    private InventorySpawner inventSpawner;

    private void Start()
    {
        inventData = FindObjectOfType<InventoryData>();
        inventSpawner = FindObjectOfType<InventorySpawner>();

        pickupButton.onClick.AddListener(OnPickUp);
    }
    private void OnPickUp()
    {
        if (inventSpawner.totalParts < inventSpawner.maxSlots)
        {
            if (chamber)
            {
                inventData.chamber += 1;
                inventSpawner.chamber += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }
            if (semiChamber)
            {
                inventData.semiChamber += 1;
                inventSpawner.semiChamber += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }
            if (autoChamber)
            {
                inventData.autoChamber += 1;
                inventSpawner.autoChamber += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }
            if (handle)
            {
                inventData.handle += 1;
                inventSpawner.handle += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }
            if (semiHandle)
            {
                inventData.semiHandle += 1;
                inventSpawner.semiHandle += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }
            if (autoHandle)
            {
                inventData.autoHandle += 1;
                inventSpawner.autoHandle += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }
            if (barrel)
            {
                inventData.barrel += 1;
                inventSpawner.barrel += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }
            if (barrelRanger)
            {
                inventData.barrelRanger += 1;
                inventSpawner.barrelRanger += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }
            if (silencerBarrel)
            {
                inventData.silencerBarrel += 1;
                inventSpawner.silencerBarrel += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }
            if (armSupport)
            {
                inventData.armSupport += 1;
                inventSpawner.armSupport += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }
            if (autoArmSupport)
            {
                inventData.autoArmSupport += 1;
                inventSpawner.autoArmSupport += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }
            if (recoilArmSupport)
            {
                inventData.recoilArmSupport += 1;
                inventSpawner.recoilArmSupport += 1;

                inventData.totalParts += 1;

                Destroy(gameObject);
            }

            //inventData.totalParts += 1;
            //
            //Destroy(gameObject);
        }
        if (inventSpawner.totalArmor < inventSpawner.maxSlots)
        {
            if (leather)
            {
                inventData.leather += 1;
                inventSpawner.leather += 1;

                inventData.totalArmor += 1;

                Destroy(gameObject);
            }
            if(iron)
            {
                inventData.iron += 1;
                inventSpawner.iron += 1;

                inventData.totalArmor += 1;

                Destroy(gameObject);
            }
            if(steel)
            {
                inventData.steel += 1;
                inventSpawner.steel += 1;

                inventData.totalArmor += 1;

                Destroy(gameObject);
            }
            if(electric)
            {
                inventData.electric += 1;
                inventSpawner.electric += 1;

                inventData.totalArmor += 1;

                Destroy(gameObject);
            }
            if(superElectric)
            {
                inventData.superElectric += 1;
                inventSpawner.superElectric += 1;

                inventData.totalArmor += 1;

                Destroy(gameObject);
            }
        
            //inventData.totalArmor += 1;
            //
            //Destroy(gameObject);
        }
        if(inventSpawner.totalMods < inventSpawner.maxSlots)
        {
            //inventData.totalMods += 1;
            //
            //Destroy(gameObject);
        }
    }
}
