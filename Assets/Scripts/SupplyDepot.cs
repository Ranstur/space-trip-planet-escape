﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SupplyDepot : MonoBehaviour
{
    [SerializeField] private bool isTesting;
    public int level;
    [Space]
    [Header("Defences")]
    [SerializeField] private List<GameObject> soldier;
    [SerializeField] private List<GameObject> heavySoldier;
    [SerializeField] private List<GameObject> turret;
    [SerializeField] private List<GameObject> rocketLauncher;
    [SerializeField] private List<GameObject> wall;
    [Header("locking bool for lists")]
    [SerializeField] private List<bool> SoldierLockingBools;
    [SerializeField] private List<bool> heavySoldierLockingBools;
    [SerializeField] private List<bool> turretLockingBools;
    [SerializeField] private List<bool> rocketLauncherLockingBools;
    [SerializeField] private List<bool> wallLockingBools;
    [Header("Public Ints")]
    public int turrets;
    public int walls;
    public int rocketlaunchers;
    public int soldiers;
    public int heavySoldiers;

    private int currentTurrets;
    private int currentWalls;
    private int currentRocketLaunchers;
    private int currentSoldiers;
    private int currentHeavySoldiers;

    //the max numbers to provent an errer.
    private int maxTurrets;
    private int maxWalls;
    private int maxRocketLaunchers;
    private int maxSoldiers;
    private int maxHeavySoldiers;

    private bool locked;

    private void Start()
    {
        maxSoldiers = soldier.Count;
        maxHeavySoldiers = heavySoldier.Count;
        maxTurrets = turret.Count;
        maxRocketLaunchers = rocketLauncher.Count;
        maxWalls = wall.Count;

        if (isTesting)
        {
            Debug.Log($"Testing mode is on! {gameObject.name}     Push U to Turn off all.");
        }
        else
        {
            foreach (GameObject allSoldiers in soldier)
            {
                allSoldiers.SetActive(false);
            }
            foreach (GameObject allHeavySoldiers in heavySoldier)
            {
                allHeavySoldiers.SetActive(false);
            }
            foreach (GameObject allTurrets in turret)
            {
                allTurrets.SetActive(false);
            }
            foreach (GameObject allRocketLaunchers in rocketLauncher)
            {
                allRocketLaunchers.SetActive(false);
            }
            foreach (GameObject allWalls in wall)
            {
                allWalls.SetActive(false);
            }
        }
    }
    private void Update()
    {
        if(isTesting)
        {
            if(Input.GetKeyDown(KeyCode.U))
            {
                isTesting = false;
                //want a reset here.
                Debug.Log("testing mode is now off! ALL");
            }
        }
        if(level >= 1 && !locked)
        {
            RanLevel();
        }
    }
    #region Random Generators

    #region Random Level
    private void RanLevel()
    {
        //loop counter
        if (currentSoldiers >= maxSoldiers && currentTurrets >= maxTurrets && currentWalls >= maxWalls && currentRocketLaunchers >= maxRocketLaunchers && currentHeavySoldiers >= maxHeavySoldiers)
        {
            level = 0;
            Debug.Log("All Items are Spawned in!");
            Debug.Log("Resetting 'Level' to Zero");
        }

        int min = 0;
        int max = 6;
        int number = Random.Range(min, max);

        if (number == 1)
        {
            if (currentSoldiers < maxSoldiers)
            {
                soldiers++;
                currentSoldiers++;
                level--;
            }
            else
            {
                RanLevel();
            }
        }

        if(number == 2)
        {
            if (currentTurrets < maxTurrets)
            {
                turrets++;
                currentTurrets++;
                level--;
            }
            else
            {
                RanLevel();
            }
        }
        if(number == 3)
        {
            if (currentWalls < maxWalls)
            {
                walls++;
                currentWalls++;
                level--;
            }
            else
            {
                RanLevel();
            }
        }
        if(number == 4)
        {
            if (currentRocketLaunchers < maxRocketLaunchers)
            {
                rocketlaunchers++;
                currentRocketLaunchers++;
                level--;
            }
            else
            {
                RanLevel();
            }
        }
        if(number == 5)
        {
            if (currentHeavySoldiers < maxHeavySoldiers)
            {
                heavySoldiers++;
                currentHeavySoldiers++;
                level--;
            }
            else
            {
                RanLevel();
            }
        }

        if(level >= 1)
        {
            RanLevel();
        }
        else
        {
            RanSoldiers();
        }
    }
    #endregion

    #region Random Soldiers
    public void RanSoldiers()
    {
        if (soldiers != 0)
        {
            int min = 0;
            int max = maxSoldiers + 1;
            int number = Random.Range(min, max);

            if (number == 1 && !SoldierLockingBools[0])
            {
                soldier[0].SetActive(true);
                SoldierLockingBools[0] = true;

                soldiers -= 1;
            }
            if (number == 2 && !SoldierLockingBools[1])
            {
                soldier[1].SetActive(true);
                SoldierLockingBools[1] = true;

                soldiers -= 1;
            }
            if (number == 3 && !SoldierLockingBools[2])
            {
                soldier[2].SetActive(true);
                SoldierLockingBools[2] = true;

                soldiers -= 1;
            }
            if (number == 4 && !SoldierLockingBools[3])
            {
                soldier[3].SetActive(true);
                SoldierLockingBools[3] = true;

                soldiers -= 1;
            }
            if (number == 5 && !SoldierLockingBools[4])
            {
                soldier[4].SetActive(true);
                SoldierLockingBools[4] = true;

                soldiers -= 1;
            }
            if (number == 6 && !SoldierLockingBools[5])
            {
                soldier[5].SetActive(true);
                SoldierLockingBools[5] = true;

                soldiers -= 1;
            }
            if (number == 7 && !SoldierLockingBools[6])
            {
                soldier[6].SetActive(true);
                SoldierLockingBools[6] = true;

                soldiers -= 1;
            }
            if (number == 8 && !SoldierLockingBools[7])
            {
                soldier[7].SetActive(true);
                SoldierLockingBools[7] = true;

                soldiers -= 1;
            }
            if (number == 9 && !SoldierLockingBools[8])
            {
                soldier[8].SetActive(true);
                SoldierLockingBools[8] = true;

                soldiers -= 1;
            }
            if (number == 10 && !SoldierLockingBools[9])
            {
                soldier[9].SetActive(true);
                SoldierLockingBools[9] = true;

                soldiers -= 1;
            }
            if (number == 11 && !SoldierLockingBools[10])
            {
                soldier[10].SetActive(true);
                SoldierLockingBools[10] = true;

                soldiers -= 1;
            }
            if (number == 12 && !SoldierLockingBools[11])
            {
                soldier[11].SetActive(true);
                SoldierLockingBools[11] = true;

                soldiers -= 1;
            }
            if (number == 13 && !SoldierLockingBools[12])
            {
                soldier[12].SetActive(true);
                SoldierLockingBools[12] = true;

                soldiers -= 1;
            }
            if (number == 14 && !SoldierLockingBools[13])
            {
                soldier[13].SetActive(true);
                SoldierLockingBools[13] = true;

                soldiers -= 1;
            }
            if (number == 15 && !SoldierLockingBools[14])
            {
                soldier[14].SetActive(true);
                SoldierLockingBools[14] = true;

                soldiers -= 1;
            }
            if (number == 16 && !SoldierLockingBools[15])
            {
                soldier[15].SetActive(true);
                SoldierLockingBools[15] = true;

                soldiers -= 1;
            }

            if (soldiers >= 1)
            {
                RanSoldiers();
            }
            else
            {
                RanHeavySoldiers();
            }
        }
        else RanHeavySoldiers();
    }
    #endregion

    #region Random Heavy Soldiers
    private void RanHeavySoldiers()
    {
        if (heavySoldiers != 0)
        {
            int min = 0;
            int max = maxHeavySoldiers + 1;
            int number = Random.Range(min, max);

            if (number == 1 && !heavySoldierLockingBools[0])
            {
                heavySoldierLockingBools[0] = true;
                heavySoldier[0].SetActive(true);

                heavySoldiers--;
            }
            if (number == 2 && !heavySoldierLockingBools[1])
            {
                heavySoldierLockingBools[1] = true;
                heavySoldier[1].SetActive(true);

                heavySoldiers--;
            }
            if (number == 3 && !heavySoldierLockingBools[2])
            {
                heavySoldierLockingBools[2] = true;
                heavySoldier[2].SetActive(true);

                heavySoldiers--;
            }
            if (number == 4 && !heavySoldierLockingBools[3])
            {
                heavySoldierLockingBools[3] = true;
                heavySoldier[3].SetActive(true);

                heavySoldiers--;
            }
            if (number == 5 && !heavySoldierLockingBools[4])
            {
                heavySoldierLockingBools[4] = true;
                heavySoldier[4].SetActive(true);

                heavySoldiers--;
            }
            if (number == 6 && !heavySoldierLockingBools[5])
            {
                heavySoldierLockingBools[5] = true;
                heavySoldier[5].SetActive(true);

                heavySoldiers--;
            }
            if (number == 7 && !heavySoldierLockingBools[6])
            {
                heavySoldierLockingBools[6] = true;
                heavySoldier[6].SetActive(true);

                heavySoldiers--;
            }
            if (number == 8 && !heavySoldierLockingBools[7])
            {
                heavySoldierLockingBools[7] = true;
                heavySoldier[7].SetActive(true);

                heavySoldiers--;
            }

            if (soldiers >= 1)
            {
                RanHeavySoldiers();
            }
            else
            {
                RanTurrets();
            }
        }
        else
        {
            RanTurrets();
        }
    }
    #endregion

    #region Random Turrets
    private void RanTurrets()
    {
        if (turrets != 0)
        {
            int min = 0;
            int max = maxTurrets + 1;
            int number = Random.Range(min, max);

            if (number == 1 && !turretLockingBools[0])
            {
                turretLockingBools[0] = true;
                turret[0].SetActive(true);

                turrets--;
            }
            if (number == 2 && !turretLockingBools[1])
            {
                turretLockingBools[1] = true;
                turret[1].SetActive(true);

                turrets--;
            }
            if (number == 3 && !turretLockingBools[2])
            {
                turretLockingBools[2] = true;
                turret[2].SetActive(true);

                turrets--;
            }
            if (number == 4 && !turretLockingBools[3])
            {
                turretLockingBools[3] = true;
                turret[3].SetActive(true);

                turrets--;
            }
            if (number == 5 && !turretLockingBools[4])
            {
                turretLockingBools[4] = true;
                turret[4].SetActive(true);

                turrets--;
            }
            if (number == 6 && !turretLockingBools[5])
            {
                turretLockingBools[5] = true;
                turret[5].SetActive(true);

                turrets--;
            }
            if (number == 7 && !turretLockingBools[6])
            {
                turretLockingBools[6] = true;
                turret[6].SetActive(true);

                turrets--;
            }
            if (number == 8 && !turretLockingBools[7])
            {
                turretLockingBools[7] = true;
                turret[7].SetActive(true);

                turrets--;
            }
            if (number == 9 && !turretLockingBools[8])
            {
                turretLockingBools[8] = true;
                turret[8].SetActive(true);

                turrets--;
            }
            if (number == 10 && !turretLockingBools[9])
            {
                turretLockingBools[9] = true;
                turret[9].SetActive(true);

                turrets--;
            }
            if (number == 11 && !turretLockingBools[10])
            {
                turretLockingBools[10] = true;
                turret[10].SetActive(true);

                turrets--;
            }
            if (number == 12 && !turretLockingBools[11])
            {
                turretLockingBools[11] = true;
                turret[11].SetActive(true);

                turrets--;
            }
            if (number == 13 && !turretLockingBools[12])
            {
                turretLockingBools[12] = true;
                turret[12].SetActive(true);

                turrets--;
            }
            if (number == 14 && !turretLockingBools[13])
            {
                turretLockingBools[13] = true;
                turret[13].SetActive(true);

                turrets--;
            }
            if (number == 15 && !turretLockingBools[14])
            {
                turretLockingBools[14] = true;
                turret[14].SetActive(true);

                turrets--;
            }
            if (number == 16 && !turretLockingBools[15])
            {
                turretLockingBools[15] = true;
                turret[15].SetActive(true);

                turrets--;
            }
            if (number == 17 && !turretLockingBools[16])
            {
                turretLockingBools[16] = true;
                turret[16].SetActive(true);

                turrets--;
            }
            if (number == 18 && !turretLockingBools[17])
            {
                turretLockingBools[17] = true;
                turret[17].SetActive(true);

                turrets--;
            }
            if (number == 19 && !turretLockingBools[18])
            {
                turretLockingBools[18] = true;
                turret[18].SetActive(true);

                turrets--;
            }
            if (number == 20 && !turretLockingBools[19])
            {
                turretLockingBools[19] = true;
                turret[19].SetActive(true);

                turrets--;
            }

            if (turrets >= 1)
            {
                RanTurrets();
            }
            else
            {
                RanRocketLauncher();
            }
        }
        else
        {
            RanRocketLauncher();
        }
    }
    #endregion

    #region Random Rocket Launchers
    private void RanRocketLauncher()
    {
        if (rocketlaunchers != 0)
        {
            int min = 0;
            int max = maxRocketLaunchers + 1;
            int number = Random.Range(min, max);

            if (number == 1 && !rocketLauncherLockingBools[0])
            {
                rocketLauncherLockingBools[0] = true;
                rocketLauncher[0].SetActive(true);

                rocketlaunchers--;
            }
            if (number == 2 && !rocketLauncherLockingBools[1])
            {
                rocketLauncherLockingBools[1] = true;
                rocketLauncher[1].SetActive(true);

                rocketlaunchers--;
            }
            if (number == 3 && !rocketLauncherLockingBools[2])
            {
                rocketLauncherLockingBools[2] = true;
                rocketLauncher[2].SetActive(true);

                rocketlaunchers--;
            }
            if (number == 4 && !rocketLauncherLockingBools[3])
            {
                rocketLauncherLockingBools[3] = true;
                rocketLauncher[3].SetActive(true);

                rocketlaunchers--;
            }
            if (number == 5 && !rocketLauncherLockingBools[4])
            {
                rocketLauncherLockingBools[4] = true;
                rocketLauncher[4].SetActive(true);

                rocketlaunchers--;
            }

            if (rocketlaunchers >= 1)
            {
                RanRocketLauncher();
            }
            else
            {
                RanWalls();
            }
        }
        else
        {
            RanWalls();
        }
    }
    #endregion

    #region Random Walls
    private void RanWalls()
    {
        if (walls != 0)
        {
            int min = 0;
            int max = maxWalls + 1;
            int number = Random.Range(min, max);

            if (number == 1 && !wallLockingBools[0])
            {
                wallLockingBools[0] = true;
                wall[0].SetActive(true);

                walls--;
            }
            if (number == 2 && !wallLockingBools[1])
            {
                wallLockingBools[1] = true;
                wall[1].SetActive(true);

                walls--;
            }
            if (number == 3 && !wallLockingBools[2])
            {
                wallLockingBools[2] = true;
                wall[2].SetActive(true);

                walls--;
            }
            if (number == 4 && !wallLockingBools[3])
            {
                wallLockingBools[3] = true;
                wall[3].SetActive(true);

                walls--;
            }
            if (number == 5 && !wallLockingBools[4])
            {
                wallLockingBools[4] = true;
                wall[4].SetActive(true);

                walls--;
            }

            if(walls >= 1)
            {
                RanWalls();
            }
        }
    }
    #endregion

    #endregion
}
