﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SecondaryObjectiveGettingAWeapon : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI objectiveNameText;
    [Header("First Objective")]
    [SerializeField] private GameObject panel1;
    [Space]
    [SerializeField] private Button pistolButton;
    [SerializeField] private Button assaultRifleButton;
    [SerializeField] private Button sniperButton;
    private bool pistol;
    private bool assaultRifle;
    private bool sniper;
    [Header("Second Objective")]
    [SerializeField] private GameObject panel2;

    private bool firstObjectiveDone;
    private bool secondObjectiveDone;

    private Data dataBanks;
    private void Start()
    {
        dataBanks = FindObjectOfType<Data>();

        firstObjectiveDone = dataBanks.firstObjectiveDone;
        secondObjectiveDone = dataBanks.secondObjectiveDone;
        pistol = dataBanks.pistol;
        assaultRifle = dataBanks.assaultRifle;
        sniper = dataBanks.sniper;

        objectiveNameText.text = "Getting A Weapon";

        pistolButton.onClick.AddListener(OnPistol);
        assaultRifleButton.onClick.AddListener(OnAssaultRifle);
        sniperButton.onClick.AddListener(OnSniper);
    }
    private void Update()
    {
        secondObjectiveDone = dataBanks.secondObjectiveDone;

        if (!firstObjectiveDone)
        {
            panel1.SetActive(true);
            panel2.SetActive(false);
        }
        if(firstObjectiveDone)
        {
            panel1.SetActive(false);
            panel2.SetActive(true);
        }
        if(secondObjectiveDone)
        {
            panel2.SetActive(false);
            //turn on the next objective
        }
    }
    private void OnPistol()
    {
        pistol = true;
        dataBanks.pistol = true;
        firstObjectiveDone = true;
        dataBanks.firstObjectiveDone = true;
    }
    private void OnAssaultRifle()
    {
        assaultRifle = true;
        dataBanks.assaultRifle = true;
        firstObjectiveDone = true;
        dataBanks.firstObjectiveDone = true;
    }
    private void OnSniper()
    {
        sniper = true;
        dataBanks.sniper = true;
        firstObjectiveDone = true;
        dataBanks.firstObjectiveDone = true;
    }
}
