﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;


public class SaveManager : MonoBehaviour
{
    private Data data;
    private LevelData levelData;

    private void Awake()
    {
        data = FindObjectOfType<Data>();
        levelData = FindObjectOfType<LevelData>();
    }

    public void Save()
    {
        Debug.Log("Saveing Game!");
        Time.timeScale = 0f;
        //Create a file or open a file to save to -- the file name must have a .dat at the end. .dat = data
        FileStream file = new FileStream(Application.persistentDataPath + "/GameData.dat", FileMode.OpenOrCreate);
        //this method is to tell you if there is an error with saving or loading the game.
        try
        {
            //Binary Formmater -- allows us to write data to a file
            BinaryFormatter formatter = new BinaryFormatter();
            //serialization method to WRITE to the file
            formatter.Serialize(file, data);
        }
        catch(SerializationException e)
        {
            Debug.LogError("Error with the saving of the data!" + e.Message);
        }
        finally
        {
            file.Close();
            Debug.Log("data Save Complete!");
        }

        FileStream file2 = new FileStream(Application.persistentDataPath + "/LevelData.dat", FileMode.OpenOrCreate);
        //this method is to tell you if there is an error with saving or loading the game.
        try
        {
            //Binary Formmater -- allows us to write data to a file
            BinaryFormatter formatter = new BinaryFormatter();
            //serialization method to WRITE to the file
            formatter.Serialize(file2, levelData);
        }
        catch (SerializationException e)
        {
            Debug.LogError("Error with the saving of the level data!" + e.Message);
        }
        finally
        {
            file.Close();
            Time.timeScale = 1f;
            Debug.Log("level Data Save Complete!");
        }
    }

    public void Load()
    {
        SceneManager.LoadScene(1);
        Debug.Log("data Loading Game!");
        FileStream file = new FileStream(Application.persistentDataPath + "/GameData.dat", FileMode.Open);

        try
        {
            BinaryFormatter formatter = new BinaryFormatter();
            //the (is name of script) 
            data = (Data) formatter.Deserialize(file);
        }
        catch(SerializationException e)
        {
            Debug.LogError("Error on loading data!" + e.Message);
        }
        finally
        {
            file.Close();
            Debug.Log("data Loading Complete!");
        }

        Debug.Log("level data Loading Game!");
        FileStream file2 = new FileStream(Application.persistentDataPath + "/GameData.dat", FileMode.Open);

        try
        {
            BinaryFormatter formatter = new BinaryFormatter();
            //the (is name of script) 
            levelData = (LevelData)formatter.Deserialize(file2);
        }
        catch (SerializationException e)
        {
            Debug.LogError("Error on loading level data!" + e.Message);
        }
        finally
        {
            file.Close();
            Debug.Log("level data Loading Complete!");
        }
    }
}
