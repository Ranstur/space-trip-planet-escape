﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipSprites : MonoBehaviour
{
    public bool crashedShip;
    public bool launchPad;
    public bool noseCone;
    public bool shipBody;
    public bool rightEngine;
    public bool mainEngine;
    public bool leftEngine;
    public bool turret;

    [SerializeField] private GameObject crashedShipObject;
    [SerializeField] private GameObject launchPadObject;
    [SerializeField] private GameObject noseConeObject;
    [SerializeField] private GameObject shipBodyObject;
    [SerializeField] private GameObject rightEngineObject;
    [SerializeField] private GameObject mainEngineObject;
    [SerializeField] private GameObject leftEngineObject;
    [SerializeField] private GameObject turretObject;

    private Data data;

    private void Start()
    {
        data = FindObjectOfType<Data>();

        crashedShipObject.SetActive(false);
        launchPadObject.SetActive(false);
        noseConeObject.SetActive(false);
        shipBodyObject.SetActive(false);
        rightEngineObject.SetActive(false);
        mainEngineObject.SetActive(false);
        leftEngineObject.SetActive(false);
        turretObject.SetActive(false);

        crashedShip = data.mainFirstObjectiveDone;
        launchPad = data.mainSecondObjectiveDone;
        noseCone = data.mainThreeObjectiveDone;
        shipBody = data.mainFourObjectiveDone;
        rightEngine = data.mainFiveObjectiveDone;
        mainEngine = data.mainSixObjectiveDone;
        leftEngine = data.mainSevenObjectiveDone;
        turret = data.mainEightObjectiveDone;
    }

    private void Update()
    {
        crashedShip = data.mainFirstObjectiveDone;
        launchPad = data.mainSecondObjectiveDone;
        noseCone = data.mainThreeObjectiveDone;
        shipBody = data.mainFourObjectiveDone;
        rightEngine = data.mainFiveObjectiveDone;
        mainEngine = data.mainSixObjectiveDone;
        leftEngine = data.mainSevenObjectiveDone;
        turret = data.mainEightObjectiveDone;

        if (!crashedShip)
        {
            crashedShipObject.SetActive(true);
        }
        if(crashedShip)
        {
            crashedShipObject.SetActive(false);
        }
        if(launchPad)
        {
            launchPadObject.SetActive(true);
        }
        if(noseCone)
        {
            noseConeObject.SetActive(true);
        }
        if(shipBody)
        {
            shipBodyObject.SetActive(true);
        }
        if(rightEngine)
        {
            rightEngineObject.SetActive(true);
        }
        if(mainEngine)
        {
            mainEngineObject.SetActive(true);
        }
        if(leftEngine)
        {
            leftEngineObject.SetActive(true);
        }
        if(turret)
        {
            turretObject.SetActive(true);
        }
    }
}
